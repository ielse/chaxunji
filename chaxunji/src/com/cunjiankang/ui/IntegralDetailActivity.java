package com.cunjiankang.ui;

import android.app.Dialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.cunjiankang.R;
import com.cunjiankang.adapter.IntegralDetailAdapter;
import com.cunjiankang.bean.request.BaseRequest;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.bean.response.IntegralDetailResponse;
import com.cunjiankang.bean.vo.IntegralDetail;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.cunjiankang.util.PreferenceUtil;
import com.cunjiankang.util.TextViewUtil;
import com.cunjiankang.view.CustomProgressDialog;
import com.google.gson.Gson;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.KJLoger;
import org.kymjs.kjframe.utils.SystemTool;

import java.util.List;

/**
 * 
 * @ClassName: IntegralDetailActivity
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:39:44
 * 
 */
public class IntegralDetailActivity extends BaseActivity {

	@BindView(id = R.id.detail_list)
	private ListView listView;
	@BindView(id = R.id.no_data)
	private TextView noData;
	@BindView(id = R.id.btn_back, click = true)
	private Button btnBack;
	@BindView(id = R.id.tv_info)
	private TextView tvInfo;
	@BindView(id = R.id.layout_content)
	private View layoutContent;
	private IntegralDetailAdapter adapter;
	private List<IntegralDetail> list;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_integral_detail);
		super.setRootView();
	}

	@Override
	public void initWidget() {
		super.initWidget();
		listView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				Contants.starTime = System.currentTimeMillis();
				return false;
			}
		});
	}

	@Override
	public void initData() {
		super.initData();

		String username = PreferenceUtil.readString(this, Contants.SP.username);
		String integral = PreferenceUtil.readString(IntegralDetailActivity.this, Contants.SP.integral);
		integral = integral == null ? "0" : integral;
		tvInfo.setText(String.format(getString(R.string.user_info), username,
				integral));
		TextViewUtil textViewUtil = new TextViewUtil();
		String info = String.format(getString(R.string.user_info), username,
				integral);
		int start1 = info.indexOf(username);
		int end1 = start1 + username.length();
		int start2 = info.indexOf(integral);
		int end2 = start2 + integral.length();
		tvInfo.setText(textViewUtil.getForegroundColorSpan(this, info, start1,
				end1, start2, end2, "#e95013"));

		final BaseRequest detailRequest = new BaseRequest();
		detailRequest.type = Contants.ApiKey.userpointdetail;
		detailRequest.cardnum = PreferenceUtil.readString(this,
				Contants.SP.cardnum, "");
		detailRequest.group_id = PreferenceUtil.readString(this, Contants.SP.local_grubid);
		String ip = PreferenceUtil.readString(this, Contants.SP.local_ip);
		String port = PreferenceUtil.readString(this, Contants.SP.local_port);
		String url = "http://" + ip + ":" + port + "/mobile/interface.do";
		final Dialog dialog = CustomProgressDialog.createDialog(this)
				.setMessage(R.string.loading);
		dialog.show();
		HttpUtil.postByLocal(IntegralDetailActivity.this,url, detailRequest, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				KJLoger.debug(detailRequest.type + ":" + t);

				try {
					IntegralDetailResponse detailResponse = new Gson().fromJson(t,
							IntegralDetailResponse.class);
					if (detailResponse.flag.equals(BaseResponse.flag_success)) {
						list = detailResponse.array;
						if (null != list && list.size() > 0) {
							adapter = new IntegralDetailAdapter(
									IntegralDetailActivity.this, list);
							listView.setAdapter(adapter);
							listView.setVisibility(View.VISIBLE);
							noData.setVisibility(View.GONE);
						} else {
							noData.setVisibility(View.VISIBLE);
							listView.setVisibility(View.GONE);
						}
					} else {
						noData.setVisibility(View.VISIBLE);
						listView.setVisibility(View.GONE);
					}
				} catch (Exception e) {
//					Toast.makeText(IntegralDetailActivity.this, "数据请求失败", Toast.LENGTH_SHORT).show();
//					skipActivity(IntegralDetailActivity.this, MainActivity.class);

					String username = PreferenceUtil.readString(IntegralDetailActivity.this, Contants.SP.username);
					String integral = PreferenceUtil.readString(IntegralDetailActivity.this, Contants.SP.integral);
					tvInfo.setText(String.format(getString(R.string.user_info), username,
							integral));
					TextViewUtil textViewUtil = new TextViewUtil();
					String info = String.format(getString(R.string.user_info), username,
							integral);
					int start1 = info.indexOf(username);
					int end1 = start1 + username.length();
					int start2 = info.indexOf(integral);
					int end2 = start2 + integral.length();
					tvInfo.setText(textViewUtil.getForegroundColorSpan(IntegralDetailActivity.this, info, start1,
							end1, start2, end2, "#e95013"));

					System.out.println(e.toString());
				}
			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				super.onFailure(t, errorNo, strMsg);
				KJLoger.debug(detailRequest.type + ":" + t.toString());
				if (Contants.debug) {
					String resp = "{\"type\":\"userconsumedetail\",\"array\":[{\"kprq\":\"2014-12-07 14\",\"ljje\":\"355\",\"descc\":\"什么什么事\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"试试事实上事实上事实上事实上事实上三生三世试试事实上事实上事实上事实上事实上是是是是\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"},{\"kprq\":\"2014-12-07 14:18:35.14\",\"ljje\":\"355\",\"descc\":\"��С��\"}],\"flag\":\"success\"}";
					IntegralDetailResponse detailResponse = new Gson()
							.fromJson(resp, IntegralDetailResponse.class);
					list = detailResponse.array;
					/*IntegralDetail titleRow = new IntegralDetail();
					titleRow.kprq = "消费时间";
					titleRow.je = "金额";
					titleRow.descc = "描述";
					list.add(0, titleRow);*/
					adapter = new IntegralDetailAdapter(
							IntegralDetailActivity.this, list);
					listView.setAdapter(adapter);
					listView.setVisibility(View.VISIBLE);
				} else {
					noData.setVisibility(View.VISIBLE);
					listView.setVisibility(View.GONE);
					if (SystemTool.checkNet(IntegralDetailActivity.this)) {

					} else {
						ViewInject.toast(getString(R.string.net_error));
					}
				}
			}

			@Override
			public void onFinish() {
				super.onFinish();
				dialog.dismiss();
				layoutContent.setVisibility(View.VISIBLE);
			}

		});
	}

	@Override
	public void widgetClick(View view) {
		super.widgetClick(view);
		Contants.starTime = System.currentTimeMillis();
		switch (view.getId()) {
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}
	}
}
