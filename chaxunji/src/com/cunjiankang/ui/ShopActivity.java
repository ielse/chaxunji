package com.cunjiankang.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import com.cunjiankang.R;
import com.cunjiankang.bean.request.ADRequest;
import com.cunjiankang.bean.response.ADResponse;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.bean.vo.ADString;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.google.gson.Gson;
import com.wangjie.imageloadersample.imageloader.CacheConfig;
import com.wangjie.imageloadersample.imageloader.ImageLoader;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.KJLoger;
import org.kymjs.kjframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Srun on 15/7/27.
 */
public class ShopActivity extends BaseActivity {
    private List<ADString> mUrlStrings;
    private ImageLoader mImageLoader;
    LayoutInflater layoutInflater;
    @BindView(id = R.id.list_view)
    ListView mListView;
    @BindView(id = R.id.btn_back, click = true)
    private Button btnBack;

    @Override
    public void setRootView() {

        setContentView(R.layout.activity_shop_activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ADRequest adRequest = new ADRequest();
        adRequest.type = Contants.ApiKey.getAD;
        adRequest.index = String.valueOf(1);

        layoutInflater = getLayoutInflater();

        ImageLoader.init(this, new CacheConfig().setDefaultResId(R.drawable.loading_1));
        mImageLoader = ImageLoader.getInstances();

        mUrlStrings = new ArrayList<ADString>();
        HttpUtil.post(adRequest, new HttpCallBack() {
            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                KJLoger.debug(t.toString());
                ADResponse response = new Gson().fromJson(t,
                        ADResponse.class);
                if (response.flag.equals(BaseResponse.flag_success)) {
                    mUrlStrings = response.array;
                    mListView.setAdapter(new BaseAdapter() {
                        @Override
                        public int getCount() {
                            return mUrlStrings.size();
                        }

                        @Override
                        public Object getItem(int position) {
                            return mUrlStrings.get(position);
                        }

                        @Override
                        public long getItemId(int position) {
                            return position;
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = layoutInflater.inflate(R.layout.list_view_item, parent, false);
                            final ImageView imageView = (ImageView) view.findViewById(R.id.shop_activity_image);
                            mImageLoader.displayImage(mUrlStrings.get(position).url, imageView, 1000);
                            return view;
                        }
                    });

                } else {
                    if (SystemTool.checkNet(ShopActivity.this)) {
                        ViewInject.toast(getString(R.string.query_data_error));
                    } else {
                        ViewInject.toast(getString(R.string.net_error));
                    }
                }
            }
        }, ShopActivity.this);
    }

    @Override
    public void widgetClick(View view) {
        super.widgetClick(view);
        switch (view.getId()) {
            case R.id.btn_back:
                finish();
                break;
            default:
                break;
        }
    }
}
