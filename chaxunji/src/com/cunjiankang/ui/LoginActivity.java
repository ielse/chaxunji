package com.cunjiankang.ui;

import android.app.Dialog;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.cunjiankang.R;
import com.cunjiankang.bean.request.BaseRequest;
import com.cunjiankang.bean.request.LoginRequest;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.bean.response.VersionResponse;
import com.cunjiankang.bean.vo.ViewControl;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.cunjiankang.util.PreferenceUtil;
import com.cunjiankang.util.UpdateService;
import com.cunjiankang.view.CustomProgressDialog;
import com.cunjiankang.view.RequestDialog;
import com.google.gson.Gson;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.KJLoger;
import org.kymjs.kjframe.utils.StringUtils;
import org.kymjs.kjframe.utils.SystemTool;

/**
 * 
 * @ClassName: LoginActivity
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:40:01
 * 
 */
public class LoginActivity extends BaseActivity implements OnClickListener {

	@BindView(id = R.id.btn_login, click = true)
	private Button btnLogin;
	@BindView(id = R.id.edit_name)
	private EditText editName;
	@BindView(id = R.id.edit_pwd)
	private EditText editPwd;
	@BindView(id = R.id.btn_card, click = true)
	private Button btnCard;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_login);
		super.setRootView();

		final BaseRequest baseRequest = new BaseRequest();
		baseRequest.type = "deploy";
		HttpUtil.post(baseRequest, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				KJLoger.debug(baseRequest.type + ":" + t);
				ViewControl viewControl = new Gson().fromJson(t, ViewControl.class);
				if (viewControl.flag.equals(BaseResponse.flag_success) && null != viewControl.array && viewControl.array.size() != 0) {
					boolean isLayoutActivityVisible = false;
					boolean isLayoutChouJiangVisible = false;
					isLayoutActivityVisible = viewControl.array.get(0).pharmacyActivitie.equals("yes");
					isLayoutChouJiangVisible=viewControl.array.get(0).consumerDraw.equals("yes");

					PreferenceUtil.write(LoginActivity.this, "is_activity_show", isLayoutActivityVisible);
					PreferenceUtil.write(LoginActivity.this,"is_choujiang_show",isLayoutChouJiangVisible);
				}
			}
		}, LoginActivity.this);
	}

	private LoginRequest checkLogin() {
		LoginRequest loginRequest = null;
		if (TextUtils.isEmpty(editName.getText())) {
			ViewInject.toast(getString(R.string.name_null));
			return null;
		} else if (TextUtils.isEmpty(editPwd.getText())) {
			ViewInject.toast(getString(R.string.pwd_null));
			return null;
		}
		loginRequest = new LoginRequest();
		loginRequest.loginid = editName.getText().toString().trim();
		loginRequest.pwd = editPwd.getText().toString().trim();
		loginRequest.type = Contants.ApiKey.login;
		return loginRequest;
	}
	
	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		switch (v.getId()) {
		case R.id.btn_login:
			final LoginRequest loginRequest = checkLogin();
			if (null != loginRequest) {
				final Dialog dialog = CustomProgressDialog.createDialog(this)
						.setMessage(R.string.loading_login);
				dialog.show();
				HttpUtil.post(loginRequest, new HttpCallBack() {
					@Override
					public void onSuccess(String t) {
						super.onSuccess(t);
						KJLoger.debug(loginRequest.type + ":" + t);
						BaseResponse baseResponse = new Gson().fromJson(t,
								BaseResponse.class);
						if (baseResponse.flag.equals(BaseResponse.flag_success)) {
							PreferenceUtil.write(LoginActivity.this,
									Contants.SP.login_id, loginRequest.loginid);
							PreferenceUtil.write(LoginActivity.this,
									Contants.SP.cardnum, baseResponse.clubcard);
							// PreferenceUtil.write(LoginActivity.this,Contants.SP.cardnum,
							// "001174");
							if(null!=baseResponse.name&&!"".equals(baseResponse.name)){
								PreferenceUtil.write(LoginActivity.this,
										Contants.SP.username, baseResponse.name);
							}else{
								PreferenceUtil.write(LoginActivity.this,
										Contants.SP.username, loginRequest.loginid);
							}
							skipActivity(LoginActivity.this, MainActivity.class);
						} else {
							String tip = Contants.errorTipMap
									.get(baseResponse.remark);
							if (StringUtils.isEmpty(tip)) {
								tip = getString(R.string.login_error);
							}
							ViewInject.toast(tip);
						}
					}

					@Override
					public void onFailure(Throwable t, int errorNo,
							String strMsg) {
						super.onFailure(t, errorNo, strMsg);
						KJLoger.debug(t.toString());
						if (SystemTool.checkNet(LoginActivity.this)) {
							ViewInject.toast(getString(R.string.login_error));
						} else {
							ViewInject.toast(getString(R.string.net_error));
						}
					}

					@Override
					public void onFinish() {
						super.onFinish();
						dialog.dismiss();
					}
				},LoginActivity.this);
			}
			break;
		case R.id.btn_card:
			skipActivity(this, CardLoginActivity.class);
			break;
		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		// editName.setText("939000");
		// editPwd.setText("123456");
		editName.setText("");
		editPwd.setText("");

	}



}
