package com.cunjiankang.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.cunjiankang.R;

import org.kymjs.kjframe.ui.BindView;

/**
 * Created by Srun on 15/6/18.
 */
public class WebActivity extends BaseActivity {
    public static String URL_STRING = "url_string";
    @BindView(id = R.id.web_view)
    WebView mWebView;
    @Override
    public void setRootView() {
        setContentView(R.layout.activity_web);
        super.setRootView();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String url = intent.getStringExtra(URL_STRING);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(false);
        webSettings.setSupportZoom(false);

        mWebView.loadUrl(url);

        mWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        ((Button) findViewById(R.id.btn_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
