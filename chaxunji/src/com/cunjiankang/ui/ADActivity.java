package com.cunjiankang.ui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.cunjiankang.R;
import com.cunjiankang.bean.request.ADRequest;
import com.cunjiankang.bean.response.ADResponse;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.bean.vo.ADString;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.google.gson.Gson;
import com.wangjie.imageloadersample.imageloader.CacheConfig;
import com.wangjie.imageloadersample.imageloader.ImageLoader;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.KJLoger;
import org.kymjs.kjframe.utils.SystemTool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Srun on 15/6/17.
 */
public class ADActivity extends BaseActivity {
    static boolean isStart;
    private List<ADString> mUrlStrings;
    int currentPosition;
    ImageLoader mImageLoader;
    ArrayList<View> viewContainter = new ArrayList<View>();

    @BindView(id = R.id.view_pager)
    ViewPager mViewPager;

    @Override
    public void setRootView() {

        setContentView(R.layout.activity_ad);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Intent intent = new Intent(this, CardLoginActivity.class);
//        startActivity(intent);
//        finish();
        ADRequest adRequest = new ADRequest();
        adRequest.type = Contants.ApiKey.getAD;
        adRequest.index = String.valueOf(1);
        isStart = true;

        ImageLoader.init(this, new CacheConfig().setDefaultResId(R.drawable.loading_1));
        mImageLoader = ImageLoader.getInstances();

        mUrlStrings = new ArrayList<ADString>();

        mViewPager.setAdapter(new PagerAdapter() {
            //viewpager中的组件数量
            @Override
            public int getCount() {
                return 1;
            }

            //滑动切换的时候销毁当前的组件
            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
            }

            //每次滑动的时候生成的组件
            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                LayoutInflater layoutInflater = getLayoutInflater();
                View view = layoutInflater.inflate(R.layout.ad_view_pager_item, null);
                final ImageView imageView = (ImageView) view.findViewById(R.id.ad_image);
                imageView.setImageResource(R.drawable.cunjiankang);
                container.addView(view);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        skipActivity(ADActivity.this, CardLoginActivity.class);
                        Contants.isADStart = false;
                        finish();
                    }
                });
                return view;
            }

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }
        });

        HttpUtil.post(adRequest, new HttpCallBack() {
            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                KJLoger.debug(t.toString());
                ADResponse response = new Gson().fromJson(t,
                        ADResponse.class);
//                if (response.flag.equals(BaseResponse.flag_success)) {
                    mUrlStrings = response.array;
                    mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int i, float v, int i1) {
                            currentPosition = i;
                        }

                        @Override
                        public void onPageSelected(int i) {

                        }

                        @Override
                        public void onPageScrollStateChanged(int i) {

                        }
                    });
                    mViewPager.setAdapter(new PagerAdapter() {
                        //viewpager中的组件数量
                        @Override
                        public int getCount() {
                            return mUrlStrings.size();
                        }

                        //滑动切换的时候销毁当前的组件
                        @Override
                        public void destroyItem(ViewGroup container, int position,
                                                Object object) {
                            container.removeView(viewContainter.get(position));
                        }

                        //每次滑动的时候生成的组件
                        @Override
                        public Object instantiateItem(ViewGroup container, final int position) {
                            LayoutInflater layoutInflater = getLayoutInflater();
                            View view = layoutInflater.inflate(R.layout.ad_view_pager_item, null);
                            final ImageView imageView = (ImageView) view.findViewById(R.id.ad_image);
                            mImageLoader.displayImage(mUrlStrings.get(position).url, imageView, 1000);
                            viewContainter.add(view);
                            container.addView(viewContainter.get(position));
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    skipActivity(ADActivity.this, CardLoginActivity.class);
                                    Contants.isADStart = false;
                                    finish();
                                }
                            });
                            return view;
                        }

                        @Override
                        public boolean isViewFromObject(View arg0, Object arg1) {
                            return arg0 == arg1;
                        }
                    });

                    new AsyncTask<Void, Void, Void>() {
                        int nextPosition;

                        @Override
                        protected Void doInBackground(Void... params) {
                            while (true) {
                                if (!isStart) {
                                    break;
                                }
                                try {
                                    Thread.sleep(5000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                if (currentPosition == mUrlStrings.size() - 1) {
                                    nextPosition = 0;
                                } else {
                                    nextPosition = currentPosition + 1;
                                }
                                System.out.println(currentPosition + "  curennt");
                                System.out.println(mUrlStrings.size() + " String size");
                                System.out.println(nextPosition + " next position");
                                mViewPager.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        mViewPager.setCurrentItem(nextPosition, true);
                                    }
                                });
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                        }
                    }.execute();
//                } else {
//                    if (SystemTool.checkNet(ADActivity.this)) {
//                        ViewInject.toast(getString(R.string.net_error));
//                    } else {
//                        ViewInject.toast(getString(R.string.query_data_error));
//                    }
//                }
            }
        }, ADActivity.this);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isStart = false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            skipActivity(ADActivity.this, CardLoginActivity.class);
            Contants.isADStart = false;
            finish();
            return false;
        } else {
            return true;
        }
    }
}
