package com.cunjiankang.ui;

import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.cunjiankang.R;
import com.cunjiankang.bean.request.BaseRequest;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.bean.response.IntegralResponse;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.cunjiankang.util.PreferenceUtil;
import com.cunjiankang.util.TextViewUtil;
import com.cunjiankang.view.CustomProgressDialog;
import com.google.gson.Gson;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.utils.KJLoger;

import java.text.DecimalFormat;

/**
 * 
 * @ClassName: IntegralActivity
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:39:27
 * 
 */
public class IntegralActivity extends BaseActivity {

	@BindView(id = R.id.layout_duihuan, click = true)
	private View layoutDuihuan;
	@BindView(id = R.id.layout_choujiang, click = true)
	private View layoutChouJiang;
	@BindView(id = R.id.layout_detail, click = true)
	private View layoutDetail;
	@BindView(id = R.id.tv_info)
	private TextView tvInfo;
	@BindView(id = R.id.btn_back, click = true)
	private Button btnBack;
	@BindView(id = R.id.img_duihuan)
	private ImageView imgDuihuan;
	@BindView(id = R.id.img_detail)
	private ImageView imgDetail;
	@BindView(id = R.id.tv_duihuan)
	private TextView tvDuihuan;
	@BindView(id = R.id.tv_detail)
	private TextView tvDetail;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_integral);
		super.setRootView();

		queryIntegral();



	}

	private void queryIntegral() {
		final Dialog dialog = CustomProgressDialog.createDialog(this, false)
				.setMessage(R.string.loading);
		dialog.show();

		final BaseRequest integral = new BaseRequest();
		integral.type = Contants.ApiKey.userpoint;
		integral.cardnum = PreferenceUtil.readString(this, Contants.SP.cardnum,
				"");
		String ip = PreferenceUtil.readString(this, Contants.SP.local_ip);
		String port = PreferenceUtil.readString(this, Contants.SP.local_port);
		String url = Contants.api_url;
		HttpUtil.postByLocal(IntegralActivity.this, url, integral, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				KJLoger.debug(integral.type + ":" + t);
				try {
					IntegralResponse integralResponse = new Gson().fromJson(t,
							IntegralResponse.class);
					if (integralResponse.flag.equals(BaseResponse.flag_success)) {
						PreferenceUtil.write(IntegralActivity.this,
								Contants.SP.integral, integralResponse.jfsyje);

						String username = PreferenceUtil.readString(IntegralActivity.this, Contants.SP.username);
						String integral = PreferenceUtil.readString(IntegralActivity.this, Contants.SP.integral);

						float price = Float.parseFloat(integral);
						DecimalFormat decimalFormat = new DecimalFormat(".000");//构造方法的字符格式这里如果小数不足2位,会以0补足.
						integral = decimalFormat.format(price);

						if (integral.equals(".000")) {
							integral = "0.000";
						}

						tvInfo.setText(String.format(getString(R.string.user_info), username,
								integral));
						TextViewUtil textViewUtil = new TextViewUtil();
						String info = String.format(getString(R.string.user_info), username,
								integral);
						int start1 = info.indexOf(username);
						int end1 = start1 + username.length();
						int start2 = info.indexOf(integral);
						int end2 = start2 + integral.length();
						tvInfo.setText(textViewUtil.getForegroundColorSpan(IntegralActivity.this, info, start1,
								end1, start2, end2, "#e95013"));
					}
				} catch (Exception e) {
//					Toast.makeText(IntegralActivity.this, "数据请求失败", Toast.LENGTH_SHORT).show();
//					skipActivity(IntegralActivity.this, MainActivity.class);
					PreferenceUtil.write(IntegralActivity.this,
							Contants.SP.integral, "0");
					String username = PreferenceUtil.readString(IntegralActivity.this, Contants.SP.username);
					String integral = "0";

					float price = Float.parseFloat(integral);
					DecimalFormat decimalFormat = new DecimalFormat(".000");//构造方法的字符格式这里如果小数不足2位,会以0补足.
					integral = decimalFormat.format(price);

					if (integral.equals(".000")) {
						integral = "0.000";
					}

					tvInfo.setText(String.format(getString(R.string.user_info), username,
							integral));
					TextViewUtil textViewUtil = new TextViewUtil();
					String info = String.format(getString(R.string.user_info), username,
							integral);
					int start1 = info.indexOf(username);
					int end1 = start1 + username.length();
					int start2 = info.indexOf(integral);
					int end2 = start2 + integral.length();
					tvInfo.setText(textViewUtil.getForegroundColorSpan(IntegralActivity.this, info, start1,
							end1, start2, end2, "#e95013"));


					System.out.println(e.toString());
				}
			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				super.onFailure(t, errorNo, strMsg);
				if (Contants.debug) {
					String resp = "{\"type\":\"userpoint\",\"jfsyje\":\"654\",\"flag\":\"success\"}";
					IntegralResponse integralResponse = new Gson().fromJson(
							resp, IntegralResponse.class);
					PreferenceUtil.write(IntegralActivity.this,
							Contants.SP.integral, integralResponse.jfsyje);
				}
				PreferenceUtil.write(IntegralActivity.this, Contants.SP.integral,
						"0");
				KJLoger.debug(integral.type + ":" + t);

				String username = PreferenceUtil.readString(IntegralActivity.this, Contants.SP.username);
				String integral = "0";

				float price = Float.parseFloat(integral);
				DecimalFormat decimalFormat = new DecimalFormat(".000");//构造方法的字符格式这里如果小数不足2位,会以0补足.
				integral = decimalFormat.format(price);

				if (integral.equals(".000")) {
					integral = "0.000";
				}

				tvInfo.setText(String.format(getString(R.string.user_info), username,
						integral));
				TextViewUtil textViewUtil = new TextViewUtil();
				String info = String.format(getString(R.string.user_info), username,
						integral);
				int start1 = info.indexOf(username);
				int end1 = start1 + username.length();
				int start2 = info.indexOf(integral);
				int end2 = start2 + integral.length();
				tvInfo.setText(textViewUtil.getForegroundColorSpan(IntegralActivity.this, info, start1,
						end1, start2, end2, "#e95013"));
			}

			@Override
			public void onFinish() {
				super.onFinish();
				dialog.dismiss();
//				String username = PreferenceUtil.readString(IntegralActivity.this,
//						Contants.SP.username);
//				TextViewUtil textViewUtil = new TextViewUtil();
//				String info = String.format(getString(R.string.welcome_info),
//						username);
//				int start = info.indexOf(username);
//				int end = start + username.length();
//				tvInfo.setText(textViewUtil.getForegroundColorSpan(
//						IntegralActivity.this, info, start, end, "#e95013"));
			}
		});


	}
	@Override
	public void initData() {
		super.initData();
		String username = PreferenceUtil.readString(IntegralActivity.this, Contants.SP.username);
		String integral = "0";

		float price = Float.parseFloat(integral);
		DecimalFormat decimalFormat = new DecimalFormat(".000");//构造方法的字符格式这里如果小数不足2位,会以0补足.
		integral = decimalFormat.format(price);

		if (integral.equals(".000")) {
			integral = "0.000";
		}

		tvInfo.setText(String.format(getString(R.string.user_info), username,
				integral));
		TextViewUtil textViewUtil = new TextViewUtil();
		String info = String.format(getString(R.string.user_info), username,
				integral);
		int start1 = info.indexOf(username);
		int end1 = start1 + username.length();
		int start2 = info.indexOf(integral);
		int end2 = start2 + integral.length();
		tvInfo.setText(textViewUtil.getForegroundColorSpan(IntegralActivity.this, info, start1,
				end1, start2, end2, "#e95013"));

	}

	@Override
	public void widgetClick(View view) {
		super.widgetClick(view);
		Contants.starTime = System.currentTimeMillis();
		Intent intent = null;
		switch (view.getId()) {
		case R.id.layout_duihuan:
			removeStyle();
			layoutDuihuan.setBackgroundResource(R.drawable.integral_btn_bg_1);
			imgDuihuan
					.setImageResource(R.drawable.integral_btn_img1_normal_new);
			tvDuihuan.setTextColor(getResources().getColor(R.color.white));
			intent = new Intent(this, IntegralDHActivity.class);
			break;
		case R.id.layout_choujiang:

			break;
		case R.id.layout_detail:
			removeStyle();
			layoutDetail.setBackgroundResource(R.drawable.integral_btn_bg_1);
			imgDetail.setImageResource(R.drawable.integral_btn_img3_normal_new);
			tvDetail.setTextColor(getResources().getColor(R.color.white));
			intent = new Intent(this, IntegralDetailActivity.class);
			break;
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}
		if (null != intent) {
			startActivity(intent);
		}
	}

	private void removeStyle() {
		layoutDetail.setBackgroundResource(R.drawable.integral_btn_bg_2);
		layoutDuihuan.setBackgroundResource(R.drawable.integral_btn_bg_2);
		imgDuihuan.setImageResource(R.drawable.integral_btn_img1_press_new);
		imgDetail.setImageResource(R.drawable.integral_btn_img3_press_new);
		tvDuihuan.setTextColor(getResources().getColor(
				R.color.health_type_press));
		tvDetail.setTextColor(getResources()
				.getColor(R.color.health_type_press));
	}
}
