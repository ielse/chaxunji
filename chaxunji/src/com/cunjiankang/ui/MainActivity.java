package com.cunjiankang.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cunjiankang.App;
import com.cunjiankang.R;
import com.cunjiankang.bean.request.BaseRequest;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.bean.response.IntegralResponse;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.cunjiankang.util.PreferenceUtil;
import com.cunjiankang.util.TextViewUtil;
import com.cunjiankang.util.Timer;
import com.cunjiankang.view.CustomProgressDialog;
import com.google.gson.Gson;

import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.utils.KJLoger;

/**
 * @author liyongqiang
 * @ClassName: MainActivity
 * @Description:
 * @date 2015-3-5 下午11:40:06
 */
@SuppressLint("HandlerLeak")
public class MainActivity extends BaseActivity {

    @BindView(id = R.id.layout_data, click = true)
    private View layoutData;
    @BindView(id = R.id.layout_integral, click = true)
    private View layoutIntegral;
    @BindView(id = R.id.layout_query, click = true)
    private View layoutQuery;
    @BindView(id = R.id.layout_record, click = true)
    private View layoutRecord;

    @BindView(id = R.id.layout_choujiang, click = true)
    private View layoutChouJiang;
    @BindView(id = R.id.layout_activity, click = true)
    private View layoutActivity;
    @BindView(id = R.id.btn_login_out, click = true)
    private Button btnLoginOut;
    @BindView(id = R.id.tv_info)
    private TextView tvInfo;

    @Override
    public void initData() {
        super.initData();

        queryIntegral();
    }



    private void queryIntegral() {
        final Dialog dialog = CustomProgressDialog.createDialog(this, false)
                .setMessage(R.string.loading);
        dialog.show();

        final BaseRequest integral = new BaseRequest();
        integral.type = Contants.ApiKey.userpoint;
        integral.cardnum = PreferenceUtil.readString(this, Contants.SP.cardnum,
                "");
        String ip = PreferenceUtil.readString(this, Contants.SP.local_ip);
        String port = PreferenceUtil.readString(this, Contants.SP.local_port);
        String url = Contants.api_url;
        HttpUtil.postByLocal(MainActivity.this, url, integral, new HttpCallBack() {
            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                KJLoger.debug(integral.type + ":" + t);
                IntegralResponse integralResponse = new Gson().fromJson(t,
                        IntegralResponse.class);
                if (integralResponse.flag.equals(BaseResponse.flag_success)) {
                    PreferenceUtil.write(MainActivity.this,
                            Contants.SP.integral, integralResponse.jfsyje);
                }
            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
                if (Contants.debug) {
                    String resp = "{\"type\":\"userpoint\",\"jfsyje\":\"654\",\"flag\":\"success\"}";
                    IntegralResponse integralResponse = new Gson().fromJson(
                            resp, IntegralResponse.class);
                    PreferenceUtil.write(MainActivity.this,
                            Contants.SP.integral, integralResponse.jfsyje);
                }
                PreferenceUtil.write(MainActivity.this, Contants.SP.integral,
                        "0");
                KJLoger.debug(integral.type + ":" + t);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
                String username = PreferenceUtil.readString(MainActivity.this,
                        Contants.SP.username);
                TextViewUtil textViewUtil = new TextViewUtil();
                String info = String.format(getString(R.string.welcome_info),
                        username);
                int start = info.indexOf(username);
                int end = start + username.length();
                tvInfo.setText(textViewUtil.getForegroundColorSpan(
                        MainActivity.this, info, start, end, "#e95013"));
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Contants.starTime = System.currentTimeMillis();
        String username = PreferenceUtil.readString(MainActivity.this,
                Contants.SP.username);
        TextViewUtil textViewUtil = new TextViewUtil();
        String info = String.format(getString(R.string.welcome_info),
                username);
        int start = info.indexOf(username);
        int end = start + username.length();
        tvInfo.setText(textViewUtil.getForegroundColorSpan(
                MainActivity.this, info, start, end, "#e95013"));

        initData();

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == Timer.MSG_CLOSE) {
                    PreferenceUtil.remove(App.i(), Contants.SP.login_id);
                    PreferenceUtil.remove(App.i(), Contants.SP.username);
                    PreferenceUtil.remove(App.i(), Contants.SP.integral);
                    PreferenceUtil.remove(App.i(), Contants.SP.cardnum);
                    skipActivity(MainActivity.this, ADActivity.class);
                    Contants.isADStart = true;
                }
            }
        };
        Timer timer = new Timer(handler);
        handler.postDelayed(timer, Timer.TIME);

//        final BaseRequest baseRequest = new BaseRequest();
//        baseRequest.type = "deploy";
//        HttpUtil.post(baseRequest, new HttpCallBack() {
//            @Override
//            public void onSuccess(String t) {
//                super.onSuccess(t);
//                KJLoger.debug(baseRequest.type + ":" + t);
//                ViewControl viewControl = new Gson().fromJson(t, ViewControl.class);
//                if (viewControl.flag.equals(BaseResponse.flag_success) && null != viewControl.array && viewControl.array.size() != 0) {
//                    layoutActivity.setVisibility(viewControl.array.get(0).pharmacyActivitie.equals("yes") ? View.VISIBLE : View.GONE);
//                    layoutChouJiang.setVisibility(viewControl.array.get(0).consumerDraw.equals("yes") ? View.VISIBLE : View.GONE);
//                }
//            }
//        }, MainActivity.this);

        layoutActivity.setVisibility(PreferenceUtil.readBoolean(this, "is_activity_show", false) ? View.VISIBLE : View.GONE);
        layoutChouJiang.setVisibility(PreferenceUtil.readBoolean(this, "is_choujiang_show", false) ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        queryVersion();
        /*
         * String username=PreferenceUtil.readString(this,
		 * Contants.SP.username); if(null==username){ queryUserInfo(); }
		 */
    }

    @Override
    public void setRootView() {
        setContentView(R.layout.activity_main);
        super.setRootView();
    }

    @Override
    public void widgetClick(View view) {
        super.widgetClick(view);
        Contants.starTime = System.currentTimeMillis();
        Intent intent = null;
        switch (view.getId()) {
            case R.id.layout_data:
                intent = new Intent(this, DataActivity.class);
                break;
            case R.id.layout_integral:
                intent = new Intent(this, IntegralActivity.class);
                break;
            case R.id.layout_query:
                intent = new Intent(this, MedicineQueryActivity.class);
                break;
            case R.id.layout_record:
                intent = new Intent(this, RecordActivity.class);
                break;
            case R.id.btn_login_out:
                PreferenceUtil.remove(App.i(), Contants.SP.login_id);
                PreferenceUtil.remove(App.i(), Contants.SP.username);
                PreferenceUtil.remove(App.i(), Contants.SP.integral);
                PreferenceUtil.remove(App.i(), Contants.SP.cardnum);
                skipActivity(MainActivity.this, CardLoginActivity.class);
                break;
            case R.id.layout_choujiang:
                String userCardNumber = PreferenceUtil.readString(this, Contants.SP.cardnum);
                String userGroupId = PreferenceUtil.readString(this, Contants.SP.local_grubid);
                String getUrl = Contants.CHOUJIANG_URL + "?" + "card=" + userCardNumber + "&groupid=" + userGroupId;
//                String getUrl = "http://mydrivers.com";
                System.out.println(getUrl);
                intent = new Intent(this,WebActivity.class);
                intent.putExtra(WebActivity.URL_STRING, getUrl);

//                intent = new Intent();
//                intent.setAction("android.intent.action.VIEW");
//                Uri content_url = Uri.parse(getUrl);
//                intent.setData(content_url);

//                intent.setClassName("com.uc.browser", "com.uc.browser.ActivityUpdate");
                break;
            case R.id.layout_activity:
                intent = new Intent(this, ShopActivity.class);
                break;
            default:
                break;
        }
        if (null != intent) {
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        PreferenceUtil.remove(this, Contants.SP.login_id);
        PreferenceUtil.remove(this, Contants.SP.username);
        PreferenceUtil.remove(this, Contants.SP.integral);
    }

}
