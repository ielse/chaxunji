package com.cunjiankang.ui;

import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.KJLoger;
import org.kymjs.kjframe.utils.SystemTool;

import android.app.Dialog;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.cunjiankang.R;
import com.cunjiankang.adapter.HealthGVAdapter;
import com.cunjiankang.bean.request.BaseRequest;
import com.cunjiankang.bean.request.LoginRequest;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.bean.response.HealthTypeResponse;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.cunjiankang.util.PreferenceUtil;
import com.cunjiankang.util.TextViewUtil;
import com.cunjiankang.view.CustomProgressDialog;
import com.google.gson.Gson;

/**
 * 
 * @ClassName: DataActivity
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:39:19
 * 
 */
public class DataActivity extends BaseActivity {

	@BindView(id = R.id.gv_data)
	private GridView gridView;
	@BindView(id = R.id.btn_back, click = true)
	private Button btnBack;
	@BindView(id = R.id.tv_info)
	private TextView tvInfo;

	private HealthGVAdapter adapter;
	private int olaSelectPoistion = -1;

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_data);
		super.setRootView();
	}

	@Override
	public void widgetClick(View view) {
		super.widgetClick(view);
		Contants.starTime = System.currentTimeMillis();
		switch (view.getId()) {
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	public void initData() {
		super.initData();

		String username = PreferenceUtil.readString(this,
				Contants.SP.username);
		TextViewUtil textViewUtil = new TextViewUtil();
		String info = String.format(getString(R.string.welcome_info),
				username);
		int start = info.indexOf(username);
		int end = start + username.length();
		tvInfo.setText(textViewUtil.getForegroundColorSpan(
				this, info, start, end,
				"#e95013"));

		BaseRequest baseRequest = new LoginRequest();
		baseRequest.loginid = PreferenceUtil.readString(this,
				Contants.SP.login_id);
		baseRequest.type = Contants.ApiKey.healthgrid;
		final Dialog dialog = CustomProgressDialog.createDialog(this)
				.setMessage(R.string.loading);
		dialog.show();
		HttpUtil.post(baseRequest, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				KJLoger.debug(t.toString());
				HealthTypeResponse response = new Gson().fromJson(t,
						HealthTypeResponse.class);
				if (response.flag.equals(BaseResponse.flag_success)) {
					adapter = new HealthGVAdapter(DataActivity.this,
							response.data);
					gridView.setAdapter(adapter);
				} else {
					if (SystemTool.checkNet(DataActivity.this)) {
						ViewInject.toast(getString(R.string.query_data_error));
					} else {
						ViewInject.toast(getString(R.string.net_error));
					}
				}
			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				super.onFailure(t, errorNo, strMsg);
				KJLoger.debug(t.toString());
				if (SystemTool.checkNet(DataActivity.this)) {
					ViewInject.toast(getString(R.string.query_data_error));
				} else {
					ViewInject.toast(getString(R.string.net_error));
				}
			}

			@Override
			public void onFinish() {
				super.onFinish();
				dialog.dismiss();
			}
		},DataActivity.this);
	}

	@Override
	public void initWidget() {
		super.initWidget();

		gridView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				Contants.starTime = System.currentTimeMillis();
				return false;
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int poistion, long arg3) {
				View layoutContent = view.findViewById(R.id.layout_content);
				TextView tvTitle = (TextView) layoutContent
						.findViewById(R.id.title);
				ImageView imgIcon = (ImageView) layoutContent
						.findViewById(R.id.icon);

				layoutContent
						.setBackgroundResource(R.drawable.data_btn_bg_press);
				layoutContent.postInvalidate();
				tvTitle.setTextColor(getResources().getColor(R.color.white));
				tvTitle.postInvalidate();

				String key = tvTitle.getTag().toString();
				Integer[] resourcesIds = Contants.healthItemIconMap.get(key);
				if (null != resourcesIds) {
					imgIcon.setImageResource(resourcesIds[1]);
					imgIcon.postInvalidate();
				}

				if (olaSelectPoistion != poistion && olaSelectPoistion >= 0) {
					View oldSelectView = gridView.getChildAt(olaSelectPoistion);
					View oldContent = oldSelectView
							.findViewById(R.id.layout_content);
					TextView oldtvTitle = (TextView) oldSelectView
							.findViewById(R.id.title);
					ImageView oldImgIcon = (ImageView) oldSelectView
							.findViewById(R.id.icon);

					oldContent
							.setBackgroundResource(R.drawable.data_btn_bg_normal);
					oldContent.postInvalidate();
					oldtvTitle.setTextColor(getResources().getColor(
							R.color.health_type_press));
					oldtvTitle.postInvalidate();
					String oldKey = oldtvTitle.getTag().toString();
					Integer[] oldResourcesIds = Contants.healthItemIconMap
							.get(oldKey);
					if (null != oldResourcesIds) {
						oldImgIcon.setImageResource(oldResourcesIds[0]);
						oldImgIcon.postInvalidate();
					}
				}
				olaSelectPoistion = poistion;
				Intent it = new Intent(DataActivity.this,
						HealthChartActivity.class);
				it.putExtra("key", key);
				showActivity(DataActivity.this, it);
			}
		});
	}
}
