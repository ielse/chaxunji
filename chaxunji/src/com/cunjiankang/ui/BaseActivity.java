package com.cunjiankang.ui;

import org.kymjs.kjframe.KJActivity;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

import com.cunjiankang.R;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.PreferenceUtil;

/**
 * 
 * @ClassName: BaseActivity
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:39:11
 * 
 */
public class BaseActivity extends KJActivity {

	@Override
	public void setRootView() {
		View view = findViewById(R.id.layout_root);
		view.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				Contants.starTime = System.currentTimeMillis();
				return false;
			}
		});
	}
	
	@Override
	public void initData() {
		// TODO Auto-generated method stub
		super.initData();
		View view=findViewById(R.id.tv_logo);
		if(null!=view){
			((TextView) view).setText(PreferenceUtil.readString(this, Contants.SP.groupName,""));
		}
	}
}
