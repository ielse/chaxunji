package com.cunjiankang.ui;

import android.os.Handler;
import android.os.Message;
import com.cunjiankang.App;
import com.cunjiankang.util.Timer;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.ui.BindView;
import org.kymjs.kjframe.ui.ViewInject;
import org.kymjs.kjframe.utils.KJLoger;
import org.kymjs.kjframe.utils.SystemTool;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.cunjiankang.R;
import com.cunjiankang.bean.request.BaseRequest;
import com.cunjiankang.bean.response.BaseResponse;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HttpUtil;
import com.cunjiankang.util.PreferenceUtil;
import com.cunjiankang.view.CustomProgressDialog;
import com.google.gson.Gson;

/**
 * 
 * @ClassName: WelcomeActivity
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:40:34
 * 
 */
public class WelcomeActivity extends BaseActivity implements OnClickListener {

	@BindView(id = R.id.btn_ok, click = true)
	private Button btnOk;
	/*@BindView(id = R.id.edit_ip)
	private EditText editIP;
	@BindView(id = R.id.edit_port)
	private EditText editPort;*/
	@BindView(id = R.id.edit_grubid)
	private EditText editGrubid;
	private Dialog dialog;

	@Override
	protected void onStart() {
		super.onStart();
		Contants.starTime = System.currentTimeMillis();
		Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (msg.what == Timer.MSG_CLOSE) {
					PreferenceUtil.remove(App.i(), Contants.SP.login_id);
					PreferenceUtil.remove(App.i(), Contants.SP.username);
					PreferenceUtil.remove(App.i(), Contants.SP.integral);
					PreferenceUtil.remove(App.i(), Contants.SP.cardnum);
					skipActivity(WelcomeActivity.this, ADActivity.class);
					Contants.isADStart = true;
				}
			}
		};
		Timer timer = new Timer(handler);
		handler.postDelayed(timer, Timer.TIME);
	}

	@Override
	public void setRootView() {
		setContentView(R.layout.activity_ip);
		super.setRootView();
		if (PreferenceUtil.readBoolean(this, Contants.SP.notFirst, false)) {
			skipActivity(this, CardLoginActivity.class);
		}
	    dialog = CustomProgressDialog.createDialog(this)
				.setMessage(R.string.loading_setting);
	}
	
	private void queryIP(String groupId){
		final BaseRequest baseRequest=new BaseRequest();
		baseRequest.type=Contants.ApiKey.getDrugip;
		baseRequest.group_id=groupId;
		HttpUtil.post(baseRequest, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				KJLoger.debug(baseRequest.type + ":" + t);
				try {
					BaseResponse baseResponse = new Gson().fromJson(t,
							BaseResponse.class);
					if (baseResponse.flag.equals(BaseResponse.flag_success)) {
						String url = baseResponse.url;
						String[] urls=url.split(":");
						String ip="";
						String port="";
						if(urls.length==2){// 不包含http 如10.340.450.23:8080
							ip = url.split(":")[0];
							port = url.split(":")[1];
						}else{// 包含http 如http://10.340.450.23:8080
							ip=url.split(":")[1].substring(2);
							port = url.split(":")[2];
						}
						
						PreferenceUtil.write(WelcomeActivity.this,
								Contants.SP.local_ip, ip);
						PreferenceUtil.write(WelcomeActivity.this,
								Contants.SP.local_port, port);
						WelcomeActivity.this.skipActivity(WelcomeActivity.this,
								CardLoginActivity.class);
					} else {
						String tip = getString(R.string.set_ip_error);
						ViewInject.toast(tip);
					}
				} catch (Exception e) {
					dialog.dismiss();
					String tip = getString(R.string.set_ip_error);
					ViewInject.toast(tip);
				}
			}
			
			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				super.onFailure(t, errorNo, strMsg);
			}
			
			@Override
			public void onFinish() {
				super.onFinish();
				dialog.dismiss();
			}
		
		},WelcomeActivity.this);
	}
	
	private void queryLogoName(String groupId){
		final BaseRequest baseRequest=new BaseRequest();
		final String systemName=getString(R.string.systemName);
		baseRequest.type=Contants.ApiKey.getGroupname;
		baseRequest.group_id=groupId;
		dialog.show();
		HttpUtil.post(baseRequest, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				KJLoger.debug(baseRequest.type + ":" + t);
				BaseResponse baseResponse = new Gson().fromJson(t,
						BaseResponse.class);
				if (baseResponse.flag.equals(BaseResponse.flag_success)) {
					PreferenceUtil.write(WelcomeActivity.this,
							Contants.SP.groupName, baseResponse.groupName+systemName);
					queryIP(baseRequest.group_id);
				} else {
					String tip = getString(R.string.set_ip_error);
					ViewInject.toast(tip);
					dialog.dismiss();
				}
			}

			@Override
			public void onFailure(Throwable t, int errorNo,
					String strMsg) {
				super.onFailure(t, errorNo, strMsg);
				KJLoger.debug(t.toString());
				if (SystemTool.checkNet(WelcomeActivity.this)) {
					ViewInject.toast(getString(R.string.set_ip_error));
				} else {
					ViewInject.toast(getString(R.string.net_error));
				}
				dialog.dismiss();
			}
		},WelcomeActivity.this);
	}

	@Override
	public void widgetClick(View v) {
		super.widgetClick(v);
		 if (TextUtils.isEmpty(editGrubid.getText())) {
			ViewInject.toast(getString(R.string.grubid_null));
			return;
		}
		SharedPreferences preferences = getSharedPreferences(
				PreferenceUtil.sp_path, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putString(Contants.SP.local_grubid, editGrubid.getText().toString().trim());
		boolean result = editor.commit();
		if (result) {
			queryLogoName(editGrubid.getText().toString().trim());
		} else {
			ViewInject.toast(getString(R.string.set_ip_error));
		}
	}
}
