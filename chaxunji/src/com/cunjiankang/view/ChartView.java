package com.cunjiankang.view;

import android.content.Context;
import android.graphics.*;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.cunjiankang.R;
import com.cunjiankang.util.Contants;
import com.cunjiankang.util.HealChatConstant;
import com.cunjiankang.view.DoubleDatePickerDialog.OnSelectScale;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author liyongqiang
 * @ClassName: ChartView
 * @Description:
 * @date 2015-3-5 下午11:41:30
 */
public class ChartView extends View {
    private static final int TITLE_HORIZONTAL_DISTANCE = 30;
    /**
     * 柱形之间的距离
     */
    private static final int PILLAR_DISTANCE = 5;
    /**
     * Y轴上刻度顶端离view的top的距离
     */
    private static final int MARGIN_TOP = 25;
    /**
     * Y轴上刻度离横线的距离
     */
    private static final int MARGIN_LEFT = 5;
    /**
     * X轴刻度与X轴的距离
     */
    private static final int MARGIN_X = 15;

    /**
     * title与矩形边框的边距
     */
    private static final int MARGIN_TITLE = 5;

    /**
     * 坐标轴上显示的值
     */
    private String mValueText;
    /**
     * 坐标轴上显示值的X坐标
     */
    private float mValueTextX;
    /**
     * 坐标轴上显示值的Y坐标
     */
    private float mValueTextY;
    /**
     * X轴刻度的横坐标
     */
    public ArrayList<Float> mScaleX = new ArrayList<Float>();

    /**
     * X轴上的刻度总数
     */
    private int mXScaleCount;
    /**
     * Y轴上的刻度总数
     */
    private int mYScaleCount;

    /**
     * Y轴上的刻度的最大值
     */
    private float mYScaleMaxValue;
    /**
     * Y轴上的刻度的最小值
     */
    private float mYScaleMinValue;

    /**
     * Y轴数据
     */
    private float[] mYScaleData;


    /**
     * X轴的刻度取值数组
     */
    private String[] mXScaleData;
    /**
     * X轴上的刻度的宽度
     */
    private float mXScaleWidth;
    /**
     * Y轴上的刻度的宽度
     */
    private float mYScaleWidth;
    private Paint mPaintLine;
    private Paint mPaintMove;
    private Paint mPaintPoint;
    private Paint mTextPaint;
    private Paint mValuePaint;
    private Paint mPillarPaint;
    private Paint mTitlePaint;

    private float leftX = -1;
    private float rightX = -1;

    private OnSelectScale onSelectScale;

    /**
     * 线行图表的总数
     */
    private int mLinearCount;
    /**
     * 线行图表的颜色数组
     */
    private int mLinearColor[] = new int[3];
    /**
     * 线行图表的标题
     */
    private String mLinearTitle[] = new String[3];
    /**
     * 每个线形点的取值
     */
    private float[][] mLinearData = new float[3][];
    /**
     * 每个线形点的取值对应的left坐标
     */
    private float[][] mLinearLeft = new float[3][];
    /**
     * 每个线形点的取值对应的right坐标
     */
    private float[][] mLinearRight = new float[3][];
    /**
     * 每个线形点的取值对应的top坐标
     */
    private float[][] mLinearTop = new float[3][];
    /**
     * 每个线形点的取值对应的bottom坐标
     */
    private float[][] mLinearBottom = new float[3][];
    /**
     * 柱形图表的总数
     */
    private int mPillarCount;
    /**
     * 柱形图表的颜色数组
     */
    private int mPillarColor[] = new int[4];
    /**
     * 柱形图表的标题
     */
    private String mPillarTitle[] = new String[4];
    /**
     * 每个柱形的取值
     */
    private float[][] mPillarData = new float[4][];
    /**
     * 每个柱形的取值对应的left坐标,用于点击的区域
     */
    private float[][] mPillarLeft = new float[4][];
    /**
     * 每个柱形的取值对应的right坐标,用于点击的区域
     */
    private float[][] mPillarRight = new float[4][];
    /**
     * 每个柱形的取值对应的top坐标,用于点击的区域
     */
    private float[][] mPillarTop = new float[4][];
    /**
     * 每个柱形的取值对应的bottom坐标,用于点击的区域
     */
    private float[][] mPillarBottom = new float[4][];
    /**
     * 每种柱形的宽
     */
    private int mPillarWidth[] = new int[4];
    private Bitmap mPointBitmap[] = new Bitmap[HealChatConstant.LINE_PONIT_RESID.length];
    /**
     * 文本的高度，估算
     */
    private float mTexteight;

    /**
     * Y轴刻度之间相差的数值
     */
    private float mYdeltaValue;
    /**
     * X轴刻度之间相差的距离
     */
    private float mXScaleInterval;
    /**
     * Y轴刻度之间相差的距离
     */
    private float mYScaleInterval;
    public boolean mError = false;

    public ChartView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ChartView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        mPaintLine = new Paint();
        mPaintLine.setAntiAlias(true);
        mPaintLine.setFakeBoldText(true);
        mPaintLine.setStrokeWidth(4f);

        mPaintMove = new Paint();
        mPaintMove.setStrokeWidth(5);
        mPaintMove.setAntiAlias(true);
        mPaintMove.setColor(0xff36B827);
        mPaintMove.setStyle(Style.STROKE);

        mPaintPoint = new Paint();
        mPaintPoint.setFakeBoldText(true);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setColor(context.getResources().getColor(
                R.color.health_chart_blue));
        mTextPaint.setFakeBoldText(true);
        mTextPaint.setTypeface(Typeface.SERIF);
        mValuePaint = new Paint();
        mValuePaint.setAntiAlias(true);
        mValuePaint.setTypeface(Typeface.SERIF);
        mValuePaint.setTextSize(20);
        mValuePaint.setColor(0xff002D4B);
        mValuePaint.setFakeBoldText(true);

        mPillarPaint = new Paint();
        mPillarPaint.setAntiAlias(true);
        mPillarPaint.setStyle(Style.FILL);
        mTitlePaint = new Paint();
        mTitlePaint.setTextSize(20);
        mTitlePaint.setAntiAlias(true);
        mTitlePaint.setColor(0xff018cdd);
        mTitlePaint.setStyle(Style.STROKE);

    }
//
//	/**
//	 * 设置X,Y轴的刻度属性
//	 *
//	 * @param xScaleData
//	 *            X轴的刻度数据
//	 * @param maxYValue
//	 *            Y轴的刻度最大值
//	 * @param minYValue
//	 *            Y轴的刻度最小值
//	 * @param yScaleCount
//	 *            Y轴的刻度总数
//	 */
//	public void setScaleAttr(String[] xScaleData, float maxYValue,
//			float minYValue, int yScaleCount) {
//		mLinearCount = 0;
//		if (null != xScaleData && xScaleData.length > 0) {
//			mXScaleData = xScaleData;
//			mXScaleCount = xScaleData.length;
//			mXScaleWidth = mTextPaint.measureText(String
//					.valueOf(xScaleData[xScaleData.length - 1]));
//		}
//		mYScaleCount = yScaleCount;
//		mYScaleMaxValue = maxYValue;
//		mYScaleMinValue = minYValue;
//		mYdeltaValue = (mYScaleMaxValue - mYScaleMinValue) / (yScaleCount - 1);
//		mTextPaint.setTextSize(10);
//		mTextPaint.setTextSize(15);
//		mYScaleWidth = mTextPaint.measureText(String.valueOf(mYScaleMaxValue));
//		FontMetrics fontMetrics = mTextPaint.getFontMetrics();
//		mTexteight = fontMetrics.descent - fontMetrics.ascent;
//
//	}

    public void setScaleAttr(String[] xScaleData, float[] yScaleData) {
        mLinearCount = 0;
        if (null != xScaleData && xScaleData.length > 0) {
            mXScaleData = xScaleData;
            mXScaleCount = xScaleData.length;
            mXScaleWidth = mTextPaint.measureText(String
                    .valueOf(xScaleData[xScaleData.length - 1]));
        }
        mYScaleCount = yScaleData.length;
        mYScaleMaxValue = yScaleData[yScaleData.length - 1];
        mYScaleMinValue = yScaleData[0];
        mYScaleData = yScaleData;
        mYdeltaValue = (mYScaleMaxValue - mYScaleMinValue) / (mYScaleCount - 1);

        mTextPaint.setTextSize(10);
        mTextPaint.setTextSize(15);
        mYScaleWidth = mTextPaint.measureText(String.valueOf(mYScaleMaxValue));
        FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTexteight = fontMetrics.descent - fontMetrics.ascent;

    }

    private float mTouchPointLeft = 0;
    private float mTouchPointTop = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Contants.starTime = System.currentTimeMillis();
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                for (int i = 0; i < mLinearCount; i++) {
                    for (int j = 0; j < mLinearData[i].length; j++) {
                        if (isInTouchArea(x, y, mLinearLeft[i][j],
                                mLinearTop[i][j], mLinearRight[i][j],
                                mLinearBottom[i][j])) {
                            mValueTextY = mLinearTop[i][j];
                            String textValue = null;
                            if (Float.valueOf(mLinearData[i][j]) % 1.0 == 0) {
                                textValue = String.valueOf(Float.valueOf(
                                        mLinearData[i][j]).intValue());
                            } else {
                                textValue = String.valueOf(Float
                                        .valueOf(mLinearData[i][j]));
                            }
                            mValueText = String.valueOf(mLinearTitle[i] + " : "
                                    + textValue);
                            if (j > 0) {
                                mValueTextX = (mLinearRight[i][j] + mLinearLeft[i][j])
                                        / 2 - mValuePaint.measureText(mValueText);
                            } else {
                                mValueTextX = mLinearRight[i][j];
                            }
                            mTouchPointLeft = mLinearLeft[i][j];
                            mTouchPointTop = mLinearTop[i][j];

                            invalidate();
                            return super.onTouchEvent(event);
                        }
                    }
                }
                leftX = x;
                mValueTextX = 0;
                mValueTextY = 0;
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                for (int i = 0; i < mLinearCount; i++) {
                    for (int j = 0; j < mLinearData[i].length; j++) {
                        if (isInTouchpoint(x, y, mLinearLeft[i][j],
                                mLinearTop[i][j], mLinearRight[i][j],
                                mLinearBottom[i][j])) {
                            rightX = mLinearTop[i][j] + 13;

                            mValueTextY = mLinearTop[i][j];
                            String textValue = null;
                            if (Float.valueOf(mLinearData[i][j]) % 1.0 == 0) {
                                textValue = String.valueOf(Float.valueOf(
                                        mLinearData[i][j]).intValue());
                            } else {
                                textValue = String.valueOf(Float
                                        .valueOf(mLinearData[i][j]));
                            }
                            mValueText = String.valueOf(mLinearTitle[i] + " : "
                                    + textValue);
                            if (j > 0) {
                                mValueTextX = (mLinearRight[i][j] + mLinearLeft[i][j])
                                        / 2 - mValuePaint.measureText(mValueText);
                            } else {
                                mValueTextX = mLinearRight[i][j];
                            }

                            mTouchPointLeft = mLinearLeft[i][j];
                            mTouchPointTop = mLinearTop[i][j];
                        }
                    }
                }
                leftX = x;

                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                float tempNum = x / getWidth();
                int num = (int) (mXScaleCount * tempNum);
                int startNum = 0,
                        endNum = mXScaleCount - 1;
                startNum = num - 7;
                endNum = num + 7;

                if (startNum <= 7) {
                    startNum = 0;
                }
                if (mXScaleCount - endNum <= 7) {
                    endNum = mXScaleCount - 1;
                }
                try {
                    if (mXScaleData == null || mXScaleData[startNum] == null
                            || mXScaleData[endNum] == null
                            || mXScaleData[num] == null) {
                        break;
                    }

                    String[] k = mXScaleData[startNum].split("/");
                    String[] l = mXScaleData[endNum].split("/");

                    calcStartDayAndEnd(2013, Integer.valueOf(k[0]) - 1,
                            Integer.valueOf(k[1]), 2013, Integer.valueOf(l[0]) - 1,
                            Integer.valueOf(l[1]) + 1);
                } catch (Exception ex) {

                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public void setXDate(String[] x, int[] xValue) {
    }

    private void calcStartDayAndEnd(int start_year, int start_monthOfYear,
                                    int start_dayOfMonth, int end_year, int end_monthOfYear,
                                    int end_dayOfMonth) {
        if (onSelectScale != null) {
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.set(start_year, start_monthOfYear, start_dayOfMonth,
                    0, 0, 0);

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set(end_year, end_monthOfYear, end_dayOfMonth, 0, 0, 0);
            onSelectScale.onDateSet(
                    String.valueOf(startCalendar.getTimeInMillis()),
                    String.valueOf(endCalendar.getTimeInMillis()));
        }
    }

    public void setLinearData(float[] data, int color, String lineartitle) {
        mLinearData[mLinearCount] = data;
        mLinearColor[mLinearCount] = color;
        mLinearTitle[mLinearCount] = lineartitle;
        mLinearLeft = new float[mLinearData.length][data.length];
        mLinearTop = new float[mLinearData.length][data.length];
        mLinearRight = new float[mLinearData.length][data.length];
        mLinearBottom = new float[mLinearData.length][data.length];
        mPointBitmap[mLinearCount] = BitmapFactory
                .decodeResource(getResources(),
                        HealChatConstant.LINE_PONIT_RESID[mLinearCount]);
        mLinearCount++;
    }

    public void setPillarData(float[] data, int color, String title) {
        mPillarData[mPillarCount] = data;
        mPillarColor[mPillarCount] = color;
        mPillarTitle[mPillarCount] = title;
        mPillarLeft = new float[mPillarData.length][data.length];
        mPillarTop = new float[mPillarData.length][data.length];
        mPillarRight = new float[mPillarData.length][data.length];
        mPillarBottom = new float[mPillarData.length][data.length];
        mPillarCount++;
        for (int i = 0; i < mPillarCount; i++) {
            mPillarWidth[i] = 200 / mPillarCount;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!mError) {
            calculateInterval();
            drawBg(canvas);
            drawFrame(canvas);

            drawmPillar(canvas);

            if (mXScaleCount > 0) {
                if (mLinearCount > 0) {
                    drawLinear(canvas);
                }
                if (0 != mValueTextX || 0 != mValueTextY) {
                    drawValue(canvas);
                }
            }
            drawTitle(canvas);
        }

        super.onDraw(canvas);
    }

    public void drawBg(Canvas canvas) {
        for (int i = 0; i < mYScaleCount - 1; i++) {
            float startY = i * mYScaleInterval + MARGIN_TOP;
            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(2);

            if (i == 0 && mYScaleData[1] == 94) {
                paint.setColor(Color.parseColor("#C8F8CB"));
            } else if (i == 0) {
                paint.setColor(Color.parseColor("#FDB5B4"));
            }

            if (i == 1 && mYScaleData[1] == 94) {
                paint.setColor(Color.parseColor("#FDB5B4"));
            } else if (i == 1) {
                paint.setColor(Color.parseColor("#C8F8CB"));
            }
            if (i == 2 && (mYScaleData[1] == 61 || mYScaleData[1] == 94)) {
                paint.setColor(Color.parseColor("#FDB5B4"));
            } else if (i == 2 && mYScaleData[1] == 40) {
                paint.setColor(Color.parseColor("#FDB5B4"));
            } else if (i == 2) {
                paint.setColor(Color.parseColor("#FDFCC6"));
            }
            if (i >= 3) {
                paint.setColor(Color.parseColor("#FDB5B4"));
            }

            float startX = mYScaleWidth + MARGIN_LEFT;
            if (mYScaleData[1] == 0.178f || mYScaleData[1] == 18.5f) {
                startX += 20f;
            }
            canvas.drawRect(startX, startY, getWidth(),
                    startY + mYScaleInterval, paint);
        }
    }

    private void calculateInterval() {
        if (mXScaleCount == 1) {
            mXScaleInterval = (this.getWidth() - (mXScaleWidth + mYScaleWidth + 4 * MARGIN_LEFT)) / 2;
        } else if (mXScaleCount < 4) {
            mXScaleInterval = (this.getWidth() / 2 - (mXScaleWidth + mYScaleWidth + 4 * MARGIN_LEFT)) / (mXScaleCount - 1);
        } else {
            mXScaleInterval = (this.getWidth() - (mXScaleWidth + mYScaleWidth + 4 * MARGIN_LEFT))
                    / (mXScaleCount - 1);
        }

        mYScaleInterval = (this.getHeight() - MARGIN_TOP - mXScaleWidth - MARGIN_X)
                / (mYScaleCount - 1);
    }

    /**
     * 绘制连框
     */
    private void drawFrame(Canvas canvas) {
        mTextPaint.setTextSize(24);
        mPaintLine.setColor(0xffB7BBBE);
        mTextPaint.setColor(0xff002D4B);

        if (leftX != -1) {
            canvas.drawLine(leftX, 0, leftX, getHeight() + 50, mPaintMove);
            if (rightX != -1)
                canvas.drawLine(0, rightX, getWidth(), rightX, mPaintMove);
        }

        for (int i = 0; i < mYScaleCount; i++) {
            float startY = i * mYScaleInterval + MARGIN_TOP;
            //TODO:修改此处，柱子文字
            String text = String.valueOf(mYScaleData[mYScaleCount - 1 - i]);
            float startX = mYScaleWidth + MARGIN_LEFT;
            if (mYScaleData[1] == 0.178f || mYScaleData[1] == 18.5f) {
                startX += 20f;
            }
            canvas.drawLine(startX, startY, getWidth(),
                    startY, mPaintLine);

            if (Float.valueOf(text) % 1.0 == 0) {
                text = String.valueOf(Float.valueOf(text).intValue());
            }
            canvas.drawText(text, 0, startY + mTexteight / 4, mTextPaint);
        }

        // 画下面的时间轴文字
        mTextPaint.setTextSize(15);
        mTextPaint.setColor(0xff002D4B);
        for (int j = 0; j < mXScaleCount; j++) {
            //
            float leftSpace;
            if (mXScaleCount == 1) {
                leftSpace = mXScaleInterval * 1 + mYScaleWidth + 3
                        * MARGIN_LEFT;
            } else if (mXScaleCount < 4) {
                leftSpace = (getWidth() / 2 - getWidth() / 4) + j * mXScaleInterval;
            } else {
                leftSpace = mXScaleInterval * j + mYScaleWidth + 3
                        * MARGIN_LEFT;
            }
            mScaleX.add(leftSpace);
            if ((mXScaleData.length - 1) >= j && null != mXScaleData[j]) {
                canvas.save();
                canvas.rotate(30, leftSpace, this.getHeight() - mXScaleWidth);
                canvas.drawText(mXScaleData[j], leftSpace, this.getHeight()
                        - mXScaleWidth, mTextPaint);
                canvas.restore();
            }
        }
    }

    /**
     * 绘制柱形
     *
     * @param canvas
     */
    private void drawmPillar(Canvas canvas) {
        int pillarSumWidth = 0;
        for (int pillarWidth : mPillarWidth) {
            pillarSumWidth += pillarWidth;
        }
        pillarSumWidth += PILLAR_DISTANCE * (mPillarWidth.length - 1);
        for (int i = 0; i < mPillarCount; i++) {
            float mYdeltaValue = mYScaleData[i + 1] - mYScaleData[i];
            mPillarPaint.setColor(mPillarColor[i]);
            for (int j = 0; j < mPillarData[i].length; j++) {
                float left = mScaleX.get(j)
                        + (mTextPaint.measureText(String
                        .valueOf(mPillarData[i][j])) - pillarSumWidth)
                        / 2;
                if (i >= 1) {
                    left = mScaleX.get(j)
                            + (mTextPaint.measureText(String
                            .valueOf(mPillarData[i][j])) - pillarSumWidth)
                            / 2 + mPillarWidth[i - 1] + PILLAR_DISTANCE * i;
                }
                float top = (MARGIN_TOP + (mYScaleMaxValue - mPillarData[i][j])
                        * mYScaleInterval / mYdeltaValue);
                canvas.drawRect(left, top, left + mPillarWidth[i],
                        (mYScaleCount - 1) * mYScaleInterval + MARGIN_TOP,
                        mPillarPaint);
                mPillarLeft[i][j] = left;
                mPillarTop[i][j] = top;
                mPillarRight[i][j] = left + mPillarWidth[i];
                mPillarBottom[i][j] = (mYScaleCount - 1) * mYScaleInterval
                        + MARGIN_TOP;
            }
        }

    }

    /**
     * 绘制线形
     *
     * @param canvas
     */
    private void drawLinear(Canvas canvas) {
        for (int i = 0; i < mLinearCount; i++) {
            mPaintLine.setColor(mLinearColor[i]);
            for (int j = 0; j < mLinearData[i].length; j++) {
                float startX = mScaleX.get(j)
                        + mTextPaint
                        .measureText(String.valueOf(mXScaleData[j]))
                        / 2;
//                float startY = (MARGIN_TOP + (mYScaleMaxValue - mLinearData[i][j]) * mYScaleInterval / mYdeltaValue);//ToDo
                float startY = getPointY(mLinearData[i][j]);
                if (j < (mLinearData[i].length - 1)) {
//                    float nextYPotion = (MARGIN_TOP + (mYScaleMaxValue - mLinearData[i][j + 1]) * mYScaleInterval / mYdeltaValue); //ToDo
                    float nextYPotion = getPointY(mLinearData[i][j + 1]);
                    canvas.drawLine(
                            startX,
                            startY,
                            mScaleX.get(j + 1)
                                    + mTextPaint.measureText(String
                                    .valueOf(mLinearData[i][j + 1]))
                                    / 2, nextYPotion, mPaintLine);
                }
                canvas.drawBitmap(mPointBitmap[i],
                        startX - mPointBitmap[i].getWidth() / 2, startY
                                - mPointBitmap[i].getHeight() / 2, mPaintPoint);
                mLinearLeft[i][j] = startX - mPointBitmap[i].getWidth() / 2;
                mLinearRight[i][j] = startX + mPointBitmap[i].getWidth() / 2;
                mLinearTop[i][j] = startY - mPointBitmap[i].getHeight() / 2;
                mLinearBottom[i][j] = startY + mPointBitmap[i].getHeight() / 2;
            }
        }

    }

    private float[] getDistanceValue() {
        float[] arrayfloat = new float[mYScaleData.length - 1];
        for (int i = 0; i < mYScaleData.length - 1; i++) {
            arrayfloat[arrayfloat.length - 1 - i] = mYScaleData[i + 1] - mYScaleData[i];
        }
        return arrayfloat;
    }

    private float[] cutYValue(float yScaleValue) {
        float tempValue = mYScaleMaxValue - yScaleValue;
        float[] arrayfloat = new float[mYScaleData.length - 1];
        float[] distances = getDistanceValue();
        for (int i = 0; i < distances.length; i++) {
            if (tempValue > distances[i]) {
                arrayfloat[i] = distances[i];
                tempValue -= distances[i];
            } else {
                arrayfloat[i] = tempValue;
                break;
            }
        }

        for (float a : arrayfloat) {
            System.out.println(a + " cut Value");
        }
        return arrayfloat;
    }

    private float getPointY(float yScaleValue) {
        float[] cutPoints = cutYValue(yScaleValue);
        float[] distance = getDistanceValue();

        float y = 0f;
        for (int i = 0; i < cutPoints.length; i++) {
            y += (cutPoints[i] / distance[i]) * mYScaleInterval;
        }
        y += MARGIN_TOP;
        return y;
    }

//    float mPointY;
//
//    private float getYDetailValue(float yScaleValue) {
//        for (int position = 0; position < mYScaleData.length; position++) {
//            if (yScaleValue <= mYScaleData[position] && position < mYScaleData.length - 1) {
//                return mYScaleData[position + 1] - mYScaleData[position];
//            }
//        }
//    }

//    private float getPointY(float yScaleValue) {
//        if (yScaleValue < mYScaleData[1] && yScaleValue >= mYScaleData[0]) {
//            mPointY += (MARGIN_TOP + (mYScaleMaxValue - yScaleValue) * getmYScaleInterval(yScaleValue) / getYDetailValue(yScaleValue));
//            return mPointY;
//
//        } else {
//            mPointY +=
//            return getPointY(yScaleValue - getDistanceValue(yScaleValue));
//        }
//    }

    /**
     * 点击时显示数值
     *
     * @param canvas
     */
    private void drawValue(Canvas canvas) {

        float x = mValueTextX;
        float y = mValueTextY
                - (mValuePaint.getFontMetrics().descent - mValuePaint
                .getFontMetrics().ascent) / 2;

        canvas.drawText(mValueText, x, y, mValuePaint);

        Paint paint = mPaintLine;
        paint.setColor(0xff002D4B);
        canvas.drawLine(mValueTextX + mValuePaint.measureText(mValueText) / 2,
                y, mTouchPointLeft, mTouchPointTop, paint);
    }

    /**
     * 画矩形titile
     *
     * @param canvas
     */
    private void drawTitle(Canvas canvas) {
        float pillarlLeft = mYScaleWidth + 3 * MARGIN_LEFT + MARGIN_TITLE;
        float titleLeft;
        float titleWidth;
        for (int m = 0; m < mLinearCount; m++) {
            String LinearTitle = mLinearTitle[m];
            titleWidth = mTitlePaint.measureText(String.valueOf(LinearTitle));
            if (!TextUtils.isEmpty(LinearTitle)) {
                float top = 0;
                titleLeft = pillarlLeft + mPointBitmap[m].getWidth()
                        + MARGIN_TITLE;
                mPaintLine.setColor(mLinearColor[m]);
                canvas.drawLine(pillarlLeft, top + 10, pillarlLeft + 20,
                        top + 10, mPaintLine);
                canvas.drawText(LinearTitle, titleLeft,
                        top + mPointBitmap[m].getHeight() / 2 + 10, mTitlePaint);
                pillarlLeft = titleLeft + titleWidth
                        + TITLE_HORIZONTAL_DISTANCE;
            }
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        for (Bitmap bitmap : mPointBitmap) {
            if (null != bitmap && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        mScaleX.clear();
        mXScaleData = null;
        for (int i = 0; i < mLinearTitle.length; i++) {
            mLinearTitle[i] = null;
        }
        for (int i = 0; i < mLinearData.length; i++) {
            mLinearData[i] = null;
        }
        for (int i = 0; i < mLinearLeft.length; i++) {
            mLinearLeft[i] = null;
        }
        for (int i = 0; i < mLinearRight.length; i++) {
            mLinearRight[i] = null;
        }
        for (int i = 0; i < mLinearTop.length; i++) {
            mLinearTop[i] = null;
        }
        for (int i = 0; i < mLinearBottom.length; i++) {
            mLinearBottom[i] = null;
        }
    }

    public void setError() {
        mError = true;
    }

    private boolean isInTouchArea(float x, float y, float left, float top,
                                  float right, float bottom) {
        return x >= (left - 15) && x <= (right + 15) && y >= (top - 15)
                && y <= (bottom + 15);
    }

    private boolean isInTouchpoint(float x, float y, float left, float top,
                                   float right, float bottom) {
        return x >= (left + 2) && x <= (right - 2);
    }

    public OnSelectScale getOnSelectScale() {
        return onSelectScale;
    }

    public void setOnSelectScale(OnSelectScale onSelectScale) {
        this.onSelectScale = onSelectScale;
    }

}
