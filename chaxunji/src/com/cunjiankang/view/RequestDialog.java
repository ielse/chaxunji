package com.cunjiankang.view;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.cunjiankang.R;

public class RequestDialog {
	private static RequestDialogListener mListener;

	public static Button cancel;

	public static void show(Context context, String message, String okText,
			String cancelText, RequestDialogListener listener) {
		mListener = listener;
		final AlertDialog dlg = new AlertDialog.Builder(context).create();
		dlg.setCancelable(false);
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.dialog_request);
		Button ok = (Button) window.findViewById(R.id.conform);
		if (null != okText) {
			ok.setText(okText);
		}
		cancel = (Button) window.findViewById(R.id.cancle);
		if (null != cancelText) {
			cancel.setText(cancelText);
		}
		TextView messageTextView = (TextView) window
				.findViewById(R.id.alertMessage);
		messageTextView.setText(message);
		ok.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dlg.cancel();
				mListener.accept(true);
			}
		});

		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dlg.cancel();
				mListener.accept(false);
			}
		});
	}

	public interface RequestDialogListener {
		public void accept(boolean accept);
	}

}
