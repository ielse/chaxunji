package com.cunjiankang.view;

import java.util.Calendar;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.Toast;

import com.cunjiankang.R;

/**
 * 
 * @ClassName: DoubleDatePickerDialog
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:41:39
 * 
 */
public class DoubleDatePickerDialog extends Dialog implements
		OnDateChangedListener {

	private Context context;
	private Button apply;
	private DatePicker startDatePicker, endDatePicker;

	private int start_year;
	private int start_monthOfYear;
	private int start_dayOfMonth;
	private int end_year;
	private int end_monthOfYear;
	private int end_dayOfMonth;

	public DoubleDatePickerDialog(Context context, int start_year,
			int start_monthOfYear, int start_dayOfMonth, int end_year,
			int end_monthOfYear, int end_dayOfMonth) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.start_year = start_year;
		this.start_monthOfYear = start_monthOfYear;
		this.start_dayOfMonth = start_dayOfMonth;
		this.end_year = end_year;
		this.end_monthOfYear = end_monthOfYear;
		this.end_dayOfMonth = end_dayOfMonth;

		init();
	}

	public DoubleDatePickerDialog(Context context, int theme, int start_year,
			int start_monthOfYear, int start_dayOfMonth, int end_year,
			int end_monthOfYear, int end_dayOfMonth) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.start_year = start_year;
		this.start_monthOfYear = start_monthOfYear;
		this.start_dayOfMonth = start_dayOfMonth;
		this.end_year = end_year;
		this.end_monthOfYear = end_monthOfYear;
		this.end_dayOfMonth = end_dayOfMonth;
		init();
	}

	private void init() {
		// this.setCanceledOnTouchOutside(true);
		this.setCancelable(true);
		this.setTitle("日期选择");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ipo_dialog_date_picker);

		initViews();
		initValues();
	}

	private void initViews() {
		apply = (Button) findViewById(R.id.apply);
		apply.setOnClickListener(clickListener);

		startDatePicker = (DatePicker) findViewById(R.id.startDatePicker);
		endDatePicker = (DatePicker) findViewById(R.id.endDatePicker);
	}

	private void initValues() {
		startDatePicker.init(start_year, start_monthOfYear, start_dayOfMonth,
				this);
		endDatePicker.init(end_year, end_monthOfYear, end_dayOfMonth, this);
	}

	private Button.OnClickListener clickListener = new Button.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.apply:
				Calendar startCalendar = Calendar.getInstance();
				startCalendar.set(start_year, start_monthOfYear,
						start_dayOfMonth, 0, 0, 0);

				Calendar endCalendar = Calendar.getInstance();
				endCalendar.set(end_year, end_monthOfYear, end_dayOfMonth, 23,
						59, 59);
				if (endCalendar.compareTo(startCalendar) < 0) {
					// end time smaller than start time.
					Log.w("TAG", "wrong time setting");
					Toast.makeText(context, R.string.wrong_time_settings,
							Toast.LENGTH_SHORT).show();
					return;
				}

				if (onDateSetListener != null) {
					onDateSetListener.onDateSet(start_year, start_monthOfYear,
							start_dayOfMonth, end_year, end_monthOfYear,
							end_dayOfMonth);
				}
				dismiss();
				break;

			default:
				break;
			}
		}
	};

	private OnDateSetListener onDateSetListener;

	public interface OnDateSetListener {
		void onDateSet(int start_year, int start_monthOfYear,
				int start_dayOfMonth, int end_year, int end_monthOfYear,
				int end_dayOfMonth);
	}

	public interface OnSelectScale {
		void onDateSet(String startTime, String endTime);
	}

	public void setOnDateSetListener(OnDateSetListener onDateSetListener) {
		this.onDateSetListener = onDateSetListener;
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		if (view == startDatePicker) {
			this.start_year = year;
			this.start_monthOfYear = monthOfYear;
			this.start_dayOfMonth = dayOfMonth;
		} else if (view == endDatePicker) {
			this.end_year = year;
			this.end_monthOfYear = monthOfYear;
			this.end_dayOfMonth = dayOfMonth;
		}
	}

}
