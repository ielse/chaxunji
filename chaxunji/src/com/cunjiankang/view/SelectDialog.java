package com.cunjiankang.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.TextView;

import com.cunjiankang.R;
import com.cunjiankang.util.Contants;

/**
 * 
 * @ClassName: SelectDialog
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:41:43
 * 
 */
public class SelectDialog {

	private Dialog mDialog;

	public SelectDialog(final Context context,
			final DialogInterfaceListener listener) {
		mDialog = new Dialog(context, R.style.CustomProgressDialog);
		View content = LayoutInflater.from(context).inflate(
				R.layout.dialog_select, null);
		content.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				Contants.starTime = System.currentTimeMillis();
				return false;
			}
		});
		mDialog.setContentView(content);

		TextView tvItem1 = (TextView) mDialog.findViewById(R.id.tx_item_1);
		TextView tvItem2 = (TextView) mDialog.findViewById(R.id.tx_item_2);
		TextView tvItem3 = (TextView) mDialog.findViewById(R.id.tx_item_3);
		TextView tvItem4 = (TextView) mDialog.findViewById(R.id.tx_item_4);

		View.OnClickListener clickListener = new OnClickListener() {

			@Override
			public void onClick(View view) {
				listener.itemClick(view);
				mDialog.dismiss();
			}
		};

		tvItem1.setOnClickListener(clickListener);
		tvItem2.setOnClickListener(clickListener);
		tvItem3.setOnClickListener(clickListener);
		tvItem4.setOnClickListener(clickListener);
	}

	public void show() {
		mDialog.setCanceledOnTouchOutside(true);
		mDialog.setCancelable(true);
		mDialog.show();
	}

	public interface DialogInterfaceListener {
		void itemClick(View view);
	}

}
