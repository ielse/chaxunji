package com.cunjiankang.util;

import org.kymjs.kjframe.utils.PreferenceHelper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 
 * @ClassName: PreferenceUtil
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:41:12
 * 
 */
public class PreferenceUtil {

	public static final String sp_path = "cjk_sp_info";

	public static void write(Context context, String k, int v) {
		PreferenceHelper.write(context, sp_path, k, v);
	}

	public static void write(Context context, String k, boolean v) {
		PreferenceHelper.write(context, sp_path, k, v);
	}

	public static void write(Context context, String k, String v) {
		PreferenceHelper.write(context, sp_path, k, v);
	}

	public static int readInt(Context context, String k) {
		SharedPreferences preference = context.getSharedPreferences(sp_path,
				Context.MODE_PRIVATE);
		return preference.getInt(k, 0);
	}

	public static int readInt(Context context, String k, int defv) {
		SharedPreferences preference = context.getSharedPreferences(sp_path,
				Context.MODE_PRIVATE);
		return preference.getInt(k, defv);
	}

	public static boolean readBoolean(Context context, String k) {
		SharedPreferences preference = context.getSharedPreferences(sp_path,
				Context.MODE_PRIVATE);
		return preference.getBoolean(k, false);
	}

	public static boolean readBoolean(Context context, String k, boolean defBool) {
		SharedPreferences preference = context.getSharedPreferences(sp_path,
				Context.MODE_PRIVATE);
		return preference.getBoolean(k, defBool);
	}

	public static String readString(Context context, String k) {
		SharedPreferences preference = context.getSharedPreferences(sp_path,
				Context.MODE_PRIVATE);
		return preference.getString(k, null);
	}

	public static String readString(Context context, String k, String defV) {
		SharedPreferences preference = context.getSharedPreferences(sp_path,
				Context.MODE_PRIVATE);
		return preference.getString(k, defV);
	}

	public static void remove(Context context, String k) {
		PreferenceHelper.remove(context, sp_path, k);
	}

	public static void clean(Context cxt) {
		PreferenceHelper.clean(cxt, sp_path);
	}
}
