package com.cunjiankang.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cunjiankang.bean.vo.HealthChartBean;
import com.cunjiankang.bean.vo.HealthChartListBean;
import com.cunjiankang.bean.vo.HealthChartBean.HealthChartBeanField;
import com.cunjiankang.bean.vo.HealthChartItemBean.HealthChartItemField;

import android.text.TextUtils;
import android.util.Log;

/**
 * @author liyongqiang
 * @ClassName: ParseResponseUtil
 * @Description:
 * @date 2015-3-5 下午11:41:08
 */
public class ParseResponseUtil {

    public static HealthChartListBean parseHealthData(String data) {
        HealthChartListBean healthChartListBean = new HealthChartListBean();
        if (TextUtils.isEmpty(data)) {
            return healthChartListBean;
        } else {
            try {
                JSONObject jsonObject = new JSONObject(data);
                healthChartListBean = new HealthChartListBean();
                healthChartListBean.type = getJSONString(jsonObject,
                        Contants.common_key_type);
                healthChartListBean.result = jsonObject
                        .getString(Contants.common_key_flag);
                healthChartListBean.objectType = jsonObject
                        .getString(Contants.object_type);

                if (Contants.common_success_str
                        .equalsIgnoreCase(healthChartListBean.result)
                        && !jsonObject.isNull(HealthChartItemField.GRAPHDETAIL)) {

                    JSONArray graphdetail = null;

                    if (healthChartListBean.objectType.equals("array")) {

                        JSONArray tempArray = jsonObject
                                .getJSONArray(HealthChartItemField.GRAPHDETAIL);

                        graphdetail = (JSONArray) tempArray.getJSONArray(0);

                    } else {

                        graphdetail = jsonObject
                                .getJSONArray(HealthChartItemField.GRAPHDETAIL);

                    }
                    Log.e("TAG", "graphdetail.length = " + graphdetail.length());

                    for (int i = 0; i < graphdetail.length(); i++) {
                        JSONObject graphdetailItem = (JSONObject) graphdetail
                                .get(i);
                        HealthChartBean bean = new HealthChartBean();
                        if (healthChartListBean.objectType.equals("array")) {
                            bean.title = getJSONString(graphdetailItem,
                                    HealthChartBeanField.NAME);
                        } else {
                            bean.title = getJSONString(graphdetailItem,
                                    HealthChartBeanField.TITLE);
                        }
                        JSONObject threeData = graphdetailItem
                                .getJSONObject(HealthChartBeanField.THREEDATA);
                        if (!threeData.isNull(HealthChartItemField.ARRAY)) {
                            JSONArray recentTitleArray = threeData
                                    .getJSONArray(HealthChartItemField.ARRAY);
                            int length = recentTitleArray.length();
                            if (recentTitleArray.length() > 3) {
                                length = 3;
                            }
                            String[] recentTitle = new String[length];
                            String[][] recentValue = new String[length][];
                            for (int n = 0; n < length; n++) {
                                JSONObject titleDesc = (JSONObject) recentTitleArray
                                        .get(n);
                                if (!titleDesc
                                        .isNull(HealthChartItemField.ARRAY)) {
                                    JSONArray recentValueArray = titleDesc
                                            .getJSONArray(HealthChartItemField.ARRAY);
                                    String value[] = new String[recentValueArray
                                            .length()];
                                    if (0 == recentValueArray.length()) {
                                        String valueStr = "";
                                        String color = "";
                                        value = new String[1];
                                        value[0] = color + valueStr;
                                    }

                                    for (int m = 0; m < recentValueArray
                                            .length(); m++) {
                                        JSONObject valueJSONObject = (JSONObject) recentValueArray
                                                .get(m);
                                        String valueStr = getJSONString(
                                                valueJSONObject,
                                                HealthChartBeanField.VALUE);
                                        String color = getJSONString(
                                                valueJSONObject,
                                                HealthChartBeanField.SIGN);
                                        value[m] = color + valueStr;
                                    }

                                    recentValue[n] = value;
                                }
                                recentTitle[n] = getJSONString(titleDesc,
                                        HealthChartBeanField.TITLE);
                            }
                            bean.recentTitle = recentTitle;
                            bean.recentValue = recentValue;
                        }

                        JSONArray array = new JSONArray();

                        if (healthChartListBean.objectType.equals("array")) {
                            array.put(graphdetailItem);
                        } else {
                            if (!graphdetailItem
                                    .isNull(HealthChartItemField.ARRAY)) {
                                array = graphdetailItem
                                        .getJSONArray(HealthChartItemField.ARRAY);
                            }
                        }

                        if (array != null) {
                            String name[] = new String[array.length()];
                            String chartType[] = new String[array.length()];
                            float yScaleValue[][] = new float[array.length()][];
                            float dataValue[][] = new float[array.length()][];
                            for (int m = 0; m < array.length(); m++) {
                                JSONObject arrayItem = (JSONObject) array
                                        .get(m);
                                name[m] = getJSONString(arrayItem,
                                        HealthChartItemField.NAME);
                                chartType[m] = getJSONString(arrayItem,
                                        HealthChartItemField.GRAPH);
                                if (!arrayItem
                                        .isNull(HealthChartItemField.AREA)) {
                                    String area = getJSONString(arrayItem,
                                            HealthChartItemField.AREA);
                                    String arearArray[] = area.split(",");
                                    float[] scaleIndex = new float[arearArray.length];
                                    for (int n = 0; n < arearArray.length; n++) {
                                        // Log.d(TAG, "arearArray[" + n +
                                        // "]=" +
                                        // arearArray[n]);
                                        scaleIndex[n] = Float
                                                .valueOf(arearArray[n]);
                                    }
                                    yScaleValue[m] = scaleIndex;
                                }
                                if (!arrayItem
                                        .isNull(HealthChartItemField.DATA)) {
                                    JSONArray dataArray = arrayItem
                                            .getJSONArray(HealthChartItemField.DATA);
                                    int length = dataArray.length();
                                    // if (dataArray.length() > 15) {
                                    // length = 15;
                                    // }
                                    String time[] = new String[length];
                                    float value[] = new float[length];
                                    for (int j = 0; j < length; j++) {
                                        JSONObject dataArrayItem = (JSONObject) dataArray
                                                .get(j);
                                        JSONObject timeObject = (JSONObject) dataArrayItem
                                                .get(HealthChartItemField.TIME);
                                        String milliseconds = getJSONString(
                                                timeObject,
                                                HealthChartItemField.TIME);
                                        time[j] = DateUtil
                                                .getHealthDateString(milliseconds);
                                        value[j] = Float.valueOf(getJSONString(
                                                dataArrayItem,
                                                HealthChartItemField.VALUE));

                                    }
                                    dataValue[m] = value;
                                    bean.time = time;
                                }
                            }
                            bean.yScaleValue = yScaleValue;
                            bean.indicatorName = name;
                            bean.chartType = chartType;
                            bean.dataValue = dataValue;

//                            float[][] tempYScaleValue = new float[1][5];
//                            tempYScaleValue[0] = new float[]{3f,5.1f,6.1f,7.1f,8.2f,33f};
//                            bean.yScaleValue = tempYScaleValue;

                        }

                        healthChartListBean.healthChartBeans.add(bean);
                    }
                }
            } catch (JSONException e) {
            }
        }
        return healthChartListBean;
    }

    protected static String getJSONString(JSONObject jsonObject, String name)
            throws JSONException {
        if (!jsonObject.isNull(name)) {
            return jsonObject.getString(name);
        } else {
            return "";
        }
    }

    protected static double getJSONDouble(JSONObject jsonObject, String name)
            throws JSONException {
        if (!jsonObject.isNull(name)) {
            return jsonObject.getDouble(name);
        } else {
            return 0;
        }
    }
}
