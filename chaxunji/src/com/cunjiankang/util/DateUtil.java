package com.cunjiankang.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;

/**
 * 
 * @ClassName: DateUtil
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:40:49
 * 
 */
@SuppressLint("SimpleDateFormat")
public class DateUtil {

	/**
	 * 日期formatter
	 */
	public static SimpleDateFormat FRIEND_MANAGER_FORMATTER = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	/**
	 * 通话时间
	 */
	public static SimpleDateFormat VOIP_TALKING_TIME = new SimpleDateFormat(
			"mm:ss");

	/**
	 * �?�?日格�?
	 */
	public static SimpleDateFormat Y_M_D = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * 时间戳格�?
	 */
	public static final SimpleDateFormat TIMESTAMP_DF = new SimpleDateFormat(
			"yyyyMMddHHmmss");

	/**
	 * [�?��话功能简述]<BR>
	 * 根据默认的格式获得当前时间字符串
	 * 
	 * @return 当前时间
	 */
	public static String getCurrentDateString() {
		return TIMESTAMP_DF.format(new Date());
	}

	/**
	 * 转化格式化的日期字符�?BR>
	 * 
	 * @param date
	 *            日期
	 * @return 格式化的日期字符�?
	 */
	public static String getDateString(Date date) {
		if (null == date) {
			return getCurrentDateString();
		}
		return TIMESTAMP_DF.format(date);
	}

	/**
	 * @param diffTime
	 *            通话时长
	 * @return 通话时长
	 */
	/**
	 * 设置通话时长格式
	 * 
	 * @param diffTime
	 *            long
	 * @param hh
	 *            �?
	 * @param mm
	 *            �?
	 * @param ss
	 *            �?
	 * @return 通话时长格式
	 */
	public static String getDiffTime(long diffTime, String hh, String mm,
			String ss) {
		// 小时常数
		long hourMarker = 60 * 60;

		// 分钟常数
		long minuteMarker = 60;

		// 秒常�?
		long secondMarker = 1;

		DecimalFormat decfmt = new DecimalFormat();
		// 小时
		long hour = diffTime / hourMarker;
		// 分钟
		long minute = (diffTime - hour * hourMarker) / minuteMarker;
		// �?
		long second = (diffTime - hour * hourMarker - minute * minuteMarker)
				/ secondMarker;

		if (hour == 0 && minute == 0) {
			return decfmt.format(second) + ss;
		}
		if (hour == 0 && minute != 0) {
			return decfmt.format(minute) + mm + decfmt.format(second) + ss;
		} else {
			return decfmt.format(hour) + hh + decfmt.format(minute) + mm
					+ decfmt.format(second) + ss;
		}
	}

	/**
	 * [�?��话功能简述]<BR>
	 * 根据默认的格式获得生日时间字符串
	 * 
	 * @param date
	 *            Date
	 * @return 生日格式时间
	 */
	public static String getBirthdayDateString(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	public static String getCheckDateString(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(date);
	}

	public static String getHealthDateString(String milliseconds) {
		Calendar cale = Calendar.getInstance();
		cale.setTimeInMillis(Long.valueOf(milliseconds));
		Date date = cale.getTime();
		SimpleDateFormat df = new SimpleDateFormat("MM/dd");
		return df.format(date);
	}

	/**
	 * yyyy-MM-dd HH-mm-ss转化为yyyy-MM-dd
	 * 
	 * @param milliseconds
	 * @return
	 * @throws ParseException
	 */
	public static String getTimeString(String source) throws ParseException {
		Date date = null;
		date = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss.SS").parse(source);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	/**
	 * 找朋友小助手时间转换<BR>
	 * 
	 * @param date
	 *            date
	 * @return 日期的时间串
	 */
	public static String getFormatTimeStringForFriendManager(Date date) {
		return null == date ? FRIEND_MANAGER_FORMATTER.format(new Date())
				: FRIEND_MANAGER_FORMATTER.format(date);
	}

	/**
	 * 找朋友小助手时间串转Date<BR>
	 * 
	 * @param friendManagerTimeString
	 *            找朋友小助手时间�?
	 * @return 日期对象
	 */
	public static Date getDateFromFriendManageTimeString(
			String friendManagerTimeString) throws ParseException {
		return FRIEND_MANAGER_FORMATTER.parse(friendManagerTimeString);
	}

	/**
	 * 获取具体时间 24小时�?
	 * 
	 * @param context
	 *            Context
	 * @param lastDate
	 *            Date
	 * @return 具体时间
	 */
	public static String getFormatClearTimeByDate(Context context, Date lastDate) {

		// 然后取出具体时间
		SimpleDateFormat dff = new SimpleDateFormat("HH:mm",
				Locale.getDefault());
		String time = dff.format(lastDate);
		return time;

	}

	/**
	 * 服务器返回的离线消息时间为UTC时间，需要转换为本地时间
	 * 
	 * @version [RCS Client V100R002C03, 2012-3-16]
	 * @param dateStr
	 *            String 解析前的字符串时间对�?
	 * @return Date 经过解析后的时间对象
	 * @exception ParseException
	 *                解析错误
	 */
	public static Date getDelayTime(String dateStr) throws ParseException {

		SimpleDateFormat imDelayFormatter = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		TimeZone timeZone = TimeZone.getTimeZone("GMT+0");
		imDelayFormatter.setTimeZone(timeZone);

		return imDelayFormatter.parse(dateStr);
	}

}
