package com.cunjiankang.util;

import android.content.Context;
import org.kymjs.kjframe.KJHttp;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpConfig;
import org.kymjs.kjframe.http.HttpParams;
import org.kymjs.kjframe.utils.KJLoger;

import com.cunjiankang.bean.request.BaseRequest;
import com.google.gson.Gson;

/**
 *
 * @ClassName: HttpUtil
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:41:04
 *
 */
public class HttpUtil {

	private static final KJHttp KJ_HTTP;
	static {
		HttpConfig config = new HttpConfig();
		config.cacheTime = 0;
		config.timeOut = 5000;
		KJ_HTTP = new KJHttp(config);

	}
	public static final String PARAMS_KEY = "content";
	public static void get(HttpCallBack callBack){
		KJ_HTTP.get(Contants.api_url,callBack);
	}
	public static void post(BaseRequest request, HttpCallBack callBack,Context context) {
		Gson gson = new Gson();
		HttpParams params = new HttpParams();

		String userGroupId = PreferenceUtil.readString(context, Contants.SP.local_grubid);
		request.group_id = userGroupId;
		String requestJson = gson.toJson(request);
		params.put(PARAMS_KEY, requestJson);
		KJLoger.debug(request.type + "请求:" + requestJson);
		KJ_HTTP.post(Contants.api_url, params, callBack);
	}

	public static void postByLocal(Context context,String url, BaseRequest request,
			HttpCallBack callBack) {
		Gson gson = new Gson();
		HttpParams params = new HttpParams();
		String userGroupId = PreferenceUtil.readString(context, Contants.SP.local_grubid);
		request.group_id = userGroupId;
		String requestJson = gson.toJson(request);
		params.put(PARAMS_KEY, requestJson);
		KJLoger.debug(request.type + "请求:" + requestJson);
		KJ_HTTP.post(url, params, callBack);
	}
	// http://192.168.1.2:8888/mobile/interface.do
}
