package com.cunjiankang.util;

import com.cunjiankang.R;

/**
 * 
 * @ClassName: HealChatConstant
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:40:59
 * 
 */
public class HealChatConstant {
	public static final int TREE_COLOR_RESID[] = { R.color.health_chart_blue,
			R.color.health_chart_red, R.color.health_chart_green };

	public static final int LINE_COLOR_RESID[] = { R.color.health_chart_blue,
			 R.color.health_chart_green ,R.color.health_chart_red};
	public static final int LINE_PONIT_RESID[] = {
			R.drawable.line_point_circle, R.drawable.line_point_red,
			R.drawable.line_point_green };
}
