package com.cunjiankang.util;

import com.cunjiankang.App;
import com.cunjiankang.R;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @ClassName: Contants
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:40:42
 * 
 */
public class Contants {

	public static boolean isADStart = false;

	public static final boolean debug = false;

	public static final String api_url = "http://cunjiankang.com/mobile/interface.do";

	public static final String CHOUJIANG_URL = "http://cunjiankang.com/sys/lottery/show.html";

	public static final String common_key_type = "type";
	public static final String common_key_apptype = "apptype";
	public static final String common_key_loginid = "loginid";
	public static final String common_push_key = "pushkey";
	public static final String common_key_flag = "flag";
	public static final String object_type = "objecttype";
	public static final String common_key_begin_time = "begintime";
	public static final String common_key_end_time = "endtime";
	public static final String common_key = "key";
	public static final String common_key_value = "value";
	public static final String common_key_remark = "remark";
	public static final String common_success_str = "success";

	public static Long starTime;

	public static final int times = (int) (2 * 60 * 1000);

	public class ApiKey {
		
		public static final String getGroupname="getGroupname";

		public static final String login = "login";

		public static final String healthgrid = "healthgrid";

		public static final String graphdetail = "graphdetail";

		public static final String jfdhsp = "jfdhsp";

		public static final String unpwdlogin = "unpwdlogin";

		public static final String userpoint = "userpoint";

		public static final String userpointdetail = "userpointdetail";

		public static final String userconsume = "userconsume";

		public static final String userconsumedetail = "userconsumedetail";

		public static final String baseinfo = "baseinfo";

		public static final String baseRequest = "baseRequest";

		public static final String getUsageDosage = "getUsageDosage";

		public static final String getUsageList = "getUsageList";

		public static final String getUsageDetail = "getUsageDetail";
		
		public static final String getCXJVersion="getCXJVersion";
		
		public static final String consume="consume";
		
		public static final String getDrugip="getDrugip";

		public static final String getAD = "AD";

	}

	public class SP {

		public static final String login_id = "login_id";

		public static final String local_ip = "local_ip";

		public static final String local_port = "local_port";

		public static final String local_grubid = "local_grubid";

		public static final String notFirst = "notFirst";

		public static final String username = "username";

		public static final String integral = "integral";

		public static final String cardnum = "cardnum";
		
		public static final String groupName="groupName";

	}

	public static final Map<String, String> errorTipMap = new HashMap<String, String>();

	static {
		errorTipMap.put("pwdfail", App.i().getString(R.string.pwd_error));
		errorTipMap.put("idnotexist", App.i().getString(R.string.name_not_find));
	}

	public static Map<String, Integer[]> healthItemIconMap = null;

	static {
		healthItemIconMap = new HashMap<String, Integer[]>();
		healthItemIconMap.put("SPO2", new Integer[] {
				R.drawable.data_item_4_press_new,
				R.drawable.data_item_4_normal_new });
		healthItemIconMap.put("PR", new Integer[] {
				R.drawable.data_item_3_press_new,
				R.drawable.data_item_3_normal_new });
		healthItemIconMap.put("SYSORDIA", new Integer[] {
				R.drawable.data_item_1_press_new,
				R.drawable.data_item_1_normal_new });
		healthItemIconMap.put("GLU", new Integer[] {
				R.drawable.data_item_2_press_new,
				R.drawable.data_item_2_normal_new });
		healthItemIconMap.put("TP", new Integer[] {
				R.drawable.data_item_7_press_new,
				R.drawable.data_item_7_normal_new });
		healthItemIconMap.put("BMI", new Integer[] {
				R.drawable.data_item_8_press_new,
				R.drawable.data_item_8_normal_new });
		healthItemIconMap.put("WEIGHT", new Integer[] {
				R.drawable.data_item_9_press_new,
				R.drawable.data_item_9_normal_new });
		healthItemIconMap.put("URICACID", new Integer[] {
				R.drawable.data_item_6_press_new,
				R.drawable.data_item_6_normal_new });
		healthItemIconMap.put("CHOLESTEROL", new Integer[] {
				R.drawable.data_item_6_press_new,
				R.drawable.data_item_6_normal_new });
	}
}
