package com.cunjiankang.util;

import android.os.Handler;

/**
 * 
 * @ClassName: Timer
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:41:24
 * 
 */
public class Timer implements Runnable {

	public static int TIME = 1000;

	public static int MSG_CLOSE = 9999;

	private Handler mHandler;

	public Timer(Handler mHandler) {
		this.mHandler = mHandler;
	}

	@Override
	public void run() {
		try {
			Long t = System.currentTimeMillis() - Contants.starTime;
			// System.out.println("ʱ��:"+t);
			if (t > Contants.times&&!Contants.isADStart) {
				mHandler.sendEmptyMessage(MSG_CLOSE);
			} else {
				mHandler.postDelayed(this, TIME);
			}
		} catch (Exception e) {

		}
	}

}
