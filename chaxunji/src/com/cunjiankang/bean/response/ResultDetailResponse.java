package com.cunjiankang.bean.response;

/**
 * 
 * @ClassName: ResultDetailResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:45
 * 
 */
public class ResultDetailResponse extends BaseResponse {

	public String sysm;

	public String gnzz;

	public String blfy;

	public String bz;

	public String yxq;
}
