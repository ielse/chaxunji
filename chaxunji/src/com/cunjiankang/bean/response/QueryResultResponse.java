package com.cunjiankang.bean.response;

import java.util.List;

import com.cunjiankang.bean.vo.QueryResult;

/**
 * 
 * @ClassName: QueryResultResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:41
 * 
 */
public class QueryResultResponse extends BaseResponse {

	public List<QueryResult> array;
}
