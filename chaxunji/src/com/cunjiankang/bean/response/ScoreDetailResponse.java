package com.cunjiankang.bean.response;

import java.util.ArrayList;

import com.cunjiankang.bean.vo.ShopDetail;

/**
 * 
 * @ClassName: ScoreDetailResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:51
 * 
 */
public class ScoreDetailResponse extends BaseResponse {

	public ArrayList<ShopDetail> array;
}
