package com.cunjiankang.bean.response;

import java.util.ArrayList;

import com.cunjiankang.bean.vo.ShopRecord;

/**
 * 
 * @ClassName: ScoreResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:55
 * 
 */
public class ScoreResponse extends BaseResponse {

	public ArrayList<ShopRecord> array;
}
