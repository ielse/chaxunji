package com.cunjiankang.bean.response;

import java.util.List;

import com.cunjiankang.bean.vo.IntegralDetail;

/**
 * 
 * @ClassName: IntegralDetailResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:27
 * 
 */
public class IntegralDetailResponse extends BaseResponse {

	public List<IntegralDetail> array;
}
