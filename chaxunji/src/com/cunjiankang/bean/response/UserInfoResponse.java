package com.cunjiankang.bean.response;

import com.cunjiankang.bean.vo.UserInfo;

/**
 * 
 * @ClassName: UserInfoResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:00
 * 
 */
public class UserInfoResponse extends BaseResponse {

	public UserInfo user;
}
