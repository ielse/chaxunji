package com.cunjiankang.bean.response;

import java.util.List;

import com.cunjiankang.bean.vo.McDetail;

/**
 * 
 * @ClassName: DrugDescResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:18
 * 
 */
public class DrugDescResponse extends BaseResponse {

	public List<McDetail> array;
}
