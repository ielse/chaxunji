package com.cunjiankang.bean.response;

/**
 * 
 * @ClassName: BaseResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:14
 * 
 */
public class BaseResponse {

	public String type;

	public String flag;

	public String remark;

	public String clubcard;

	public String name;
	
	public String groupName;
	
	public String je;
	
	public String url;

	public String version;

	public static final String flag_success = "success";

	public static final String flag_fail = "fail";
}
