package com.cunjiankang.bean.response;

import java.util.ArrayList;

import com.cunjiankang.bean.vo.HealthType;

/**
 * 
 * @ClassName: HealthTypeResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:23
 * 
 */
public class HealthTypeResponse extends BaseResponse {

	public ArrayList<HealthType> data;
}
