package com.cunjiankang.bean.response;

import java.util.List;

import com.cunjiankang.bean.vo.Goods;

/**
 * 
 * @ClassName: IntegralGoodResponse
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:37:31
 * 
 */
public class IntegralGoodResponse extends BaseResponse {

	public List<Goods> array;
}
