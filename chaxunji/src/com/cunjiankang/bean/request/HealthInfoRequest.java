package com.cunjiankang.bean.request;

/**
 * 
 * @ClassName: HealthInfoRequest
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:36:46
 * 
 */
public class HealthInfoRequest extends BaseRequest {

	public String timetype;

	public String begintime;

	public String endtime;

	public String key;
}
