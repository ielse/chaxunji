package com.cunjiankang.bean.request;

/**
 * 
 * @ClassName: CardLoginRequest
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:36:25
 * 
 */
public class CardLoginRequest extends BaseRequest {

	public String card;
}
