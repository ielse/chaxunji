package com.cunjiankang.bean.request;

/**
 * 
 * @ClassName: BaseRequest
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:36:17
 * 
 */
public class BaseRequest {

	public String type;

	public String apptype = "android";

	public String loginid;

	public String cardnum;

	public String card;

	public String id;
	
	public String group_id;
}
