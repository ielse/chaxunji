package com.cunjiankang.bean.vo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @ClassName: HealthChartListBean
 * @Description: 图表集合bean
 * @author liyongqiang
 * @date 2015-2-1 下午10:26:47
 * 
 */
public class HealthChartListBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public interface HealthChartListField {
		public static final String HEALTHCHART = "healthchart";
		public static final String TYPEID = "typeid";
		public static final String NAME = "name";
		public static final String UNIT = "unit";

	}

	public String type;
	public String result;
	public String objectType;

	public ArrayList<HealthChartBean> healthChartBeans = new ArrayList<HealthChartBean>();

}
