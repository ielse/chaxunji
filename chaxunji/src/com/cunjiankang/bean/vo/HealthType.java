package com.cunjiankang.bean.vo;

/**
 * 
 * @ClassName: HealthType
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:32
 * 
 */
public class HealthType {

	public String name;
	public String icon;
	public String key;
}
