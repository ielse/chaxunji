package com.cunjiankang.bean.vo;

import java.io.Serializable;

/**
 * 
 * @ClassName: HealthChartItemBean
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:21
 * 
 */
public class HealthChartItemBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public interface HealthChartItemField {
		public static final String GRAPHDETAIL = "graphdetail";
		public static final String ARRAY = "array";
		public static final String AREA = "area";
		public static final String GRAPH = "graph";
		public static final String DATA = "data";
		public static final String TIME = "time";
		public static final String MONTH = "month";
		public static final String DATE = "date";
		public static final String VALUE = "value";
		public static final String NAME = "name";
	}

	public int chartType;
	public String typeId;

	public String type;
	public String result;
	public String time;
	public String value;
}
