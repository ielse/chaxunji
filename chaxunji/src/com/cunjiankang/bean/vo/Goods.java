package com.cunjiankang.bean.vo;

/**
 * 
 * @ClassName: Goods
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:09
 * 
 */
public class Goods {

	public String id;

	public String name;

	public String content;

	public String img;
}
