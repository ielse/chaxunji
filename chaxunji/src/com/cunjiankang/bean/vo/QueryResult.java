package com.cunjiankang.bean.vo;

/**
 * 
 * @ClassName: QueryResult
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:41
 * 
 */
public class QueryResult {

	public String id;

	public String sccj;

	public String spmc;
	
	public String spec;
	
	public String unit;
}
