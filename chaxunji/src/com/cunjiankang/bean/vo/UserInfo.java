package com.cunjiankang.bean.vo;

/**
 * 
 * @ClassName: UserInfo
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:39:03
 * 
 */
public class UserInfo {

	public String name;
	public String sex;
	public String usertype;
	public String age;
	public String corp;
	public String lastphytime;
	public String lastphydetail;
	public String phyvalue;
}
