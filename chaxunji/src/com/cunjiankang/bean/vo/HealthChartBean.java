package com.cunjiankang.bean.vo;

import java.io.Serializable;

/**
 * 
 * @ClassName: HealthChartBean
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:16
 * 
 */
public class HealthChartBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public interface HealthChartBeanField {
		public static final String TITLE = "desc";
		public static final String NAME = "name";
		public static final String THREEDATA = "threedata";
		public static final String VALUE = "value";
		public static final String SIGN = "sign";
	}

	public String typeId;

	public String type;
	public String result;
	public String title;
	public String recentTitle[];
	public String recentValue[][];
	public String[] time;
	public float yScaleValue[][];
	public float[][] dataValue;
	public String[] indicatorName;
	public String[] chartType;
}
