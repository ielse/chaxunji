package com.cunjiankang.bean.vo;

/**
 * 
 * @ClassName: ShopDetail
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:49
 * 
 */
public class ShopDetail {

	public String amount;

	public String unit;

	public String price;

	public String xsmc;
}
