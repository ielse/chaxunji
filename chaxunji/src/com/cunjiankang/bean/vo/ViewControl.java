package com.cunjiankang.bean.vo;

import java.util.List;

/**
 * Created by Srun on 15/9/6.
 */
public class ViewControl {

    /**
     * type : deploy
     * flag : success
     * array : [{"detectionData":"yes","consumptionPoint":"yes","pharmacyActivitie":"yes","drugQuery":"yes","consumerRecord":"yes","consumerDraw":"yes"}]
     */

    public String type;
    public String flag;
    public List<ArrayEntity> array;



    public static class ArrayEntity {
        /**
         * detectionData : yes
         * consumptionPoint : yes
         * pharmacyActivitie : yes
         * drugQuery : yes
         * consumerRecord : yes
         * consumerDraw : yes
         */

        public String detectionData;
        public String consumptionPoint;
        public String pharmacyActivitie;
        public String drugQuery;
        public String consumerRecord;
        public String consumerDraw;
    }
}
