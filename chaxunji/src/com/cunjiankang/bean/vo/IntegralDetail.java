package com.cunjiankang.bean.vo;

/**
 * 
 * @ClassName: IntegralDetail
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:38:37
 * 
 */
public class IntegralDetail {

	public String kprq;

	public String ljje;

	public String descc;

	public String je;

	public String xzjf;
}
