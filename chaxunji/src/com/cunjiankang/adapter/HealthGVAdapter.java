package com.cunjiankang.adapter;

import java.util.List;

import com.cunjiankang.R;
import com.cunjiankang.bean.vo.HealthType;
import com.cunjiankang.util.Contants;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @ClassName: HealthGVAdapter
 * @Description: 
 * @author liyongqiang
 * @date 2015-3-5 下午11:34:43
 * 
 */
public class HealthGVAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<HealthType> list;

	public HealthGVAdapter(Context context, List<HealthType> list) {
		inflater = LayoutInflater.from(context);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int poistion) {
		return list.get(poistion);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int poistion, View contentView, ViewGroup parent) {
		HealthType healthType = list.get(poistion);
		ViewCache cache = null;
		if (null == contentView) {
			contentView = inflater.inflate(R.layout.item_data, null);
			cache = new ViewCache();
			cache.title = (TextView) contentView.findViewById(R.id.title);
			cache.icon = (ImageView) contentView.findViewById(R.id.icon);
			contentView.setTag(cache);
		} else {
			cache = (ViewCache) contentView.getTag();
		}
		cache.title.setText(healthType.name);
		cache.title.setTag(healthType.key);
		Integer[] resourcesIds = Contants.healthItemIconMap.get(healthType.key);
		if (null != resourcesIds) {
			cache.icon.setImageResource(resourcesIds[0]);
		}
		return contentView;
	}

	class ViewCache {
		TextView title;
		ImageView icon;
	}
}
