package com.cunjiankang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.cunjiankang.R;
import com.cunjiankang.bean.vo.McDetail;

import java.text.DecimalFormat;
import java.util.List;

/**
 * 
 * @ClassName: ScoreAdapter
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:36:02
 * 
 */
public class McInfoAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<McDetail> list;
	private Context context;

	public McInfoAdapter(Context context, List<McDetail> list) {
		inflater = LayoutInflater.from(context);
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int poistion) {
		return list.get(poistion);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int poistion, View contentView, ViewGroup parent) {
		McDetail detail = list.get(poistion);
		ViewCache cache = null;
		if (null == contentView) {
			contentView = inflater.inflate(R.layout.item_mc_info, null);
			cache = new ViewCache();
			cache.tvSppc = (TextView) contentView.findViewById(R.id.tv_spmc);
			cache.tvSpgg = (TextView) contentView.findViewById(R.id.tv_spgg);
			cache.tvDw = (TextView) contentView.findViewById(R.id.tv_dw);
			cache.tvDj = (TextView) contentView.findViewById(R.id.tv_dj);
			cache.tvSl = (TextView) contentView.findViewById(R.id.tv_shu);
			cache.layoutContent = contentView.findViewById(R.id.layout_content);
			contentView.setTag(cache);
		} else {
			cache = (ViewCache) contentView.getTag();
		}
		
		if(detail.sshje==null){
			detail.sshje="";
		}else {
			double dj = 0l;
			try {
				dj = Double.parseDouble(detail.sshje);
			} catch (Exception e) {
				dj = 0l;
			}
			DecimalFormat df = new DecimalFormat("#.00");
			detail.sshje = df.format(dj);
		}
		
		if(detail.shl==null){
			detail.shl="0";
		}else{
			double shl=Double.parseDouble(detail.shl);
			DecimalFormat df = new DecimalFormat("#.0");
			detail.shl=df.format(shl);
		}
		
		cache.tvSppc.setText(null == detail.spmch ? "" : detail.spmch.trim());
		cache.tvSpgg.setText(null == detail.shpgg ? "" : detail.shpgg.trim());
		cache.tvDw.setText(null == detail.dw ? "" : detail.dw.trim());
		cache.tvDj.setText(detail.sshje);
		cache.tvSl.setText(detail.shl);

		cache.layoutContent.setBackgroundResource(R.drawable.tr_border);
		cache.tvSppc.setBackgroundResource(R.drawable.td_border);
		cache.tvSpgg.setBackgroundResource(R.drawable.td_border);
		cache.tvDw.setBackgroundResource(R.drawable.td_border);
		cache.tvDj.setBackgroundResource(R.drawable.td_border);
		cache.tvSl.setBackgroundResource(R.drawable.td_border);

		cache.tvSppc.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvSpgg.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvDw.setTextColor(context.getResources().getColor(
				R.color.black));
		cache.tvDj.setTextColor(context.getResources().getColor(
				R.color.black));
		cache.tvSl.setTextColor(context.getResources().getColor(
				R.color.black));

		return contentView;
	}

	class ViewCache {
		TextView tvSppc;
		TextView tvSpgg;
		TextView tvDw;
		TextView tvDj;
		TextView tvSl;
		View layoutContent;
	}
}
