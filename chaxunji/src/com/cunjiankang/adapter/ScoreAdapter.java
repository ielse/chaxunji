package com.cunjiankang.adapter;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cunjiankang.R;
import com.cunjiankang.bean.vo.ShopRecord;
import com.cunjiankang.ui.RecordActivity;

/**
 * 
 * @ClassName: ScoreAdapter
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:36:02
 * 
 */
public class ScoreAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<ShopRecord> list;
	private Context context;

	public ScoreAdapter(Context context, List<ShopRecord> list) {
		inflater = LayoutInflater.from(context);
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int poistion) {
		return list.get(poistion);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View getView(int poistion, View contentView, ViewGroup parent) {
		final ShopRecord record = list.get(poistion);
		ViewCache cache = null;
		if (null == contentView) {
			contentView = inflater.inflate(R.layout.item_shop_record, null);
			cache = new ViewCache();
			cache.tvLshh = (TextView) contentView.findViewById(R.id.tv_lshh);
			cache.tvTime = (TextView) contentView.findViewById(R.id.tv_time);
			cache.tvZje = (TextView) contentView.findViewById(R.id.tv_zje);
			cache.tvDesc = (TextView) contentView.findViewById(R.id.tv_desc);
			cache.layoutContent = contentView.findViewById(R.id.layout_content);
			contentView.setTag(cache);
		} else {
			cache = (ViewCache) contentView.getTag();
		}
		
		if(record.zje==null){
			record.zje="";
		}else{
			double zje=Double.parseDouble(record.zje);
			DecimalFormat df = new DecimalFormat("#.00");
			record.zje=df.format(zje);
		}
		
		cache.tvLshh.setText(null == record.lshh ? "" : record.lshh.trim());
		String time=record.rq+" "+record.ontime;
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			time=sdf.format(sdf.parse(time));
		} catch (ParseException e) {
			time=record.rq+" "+record.ontime;
		}
		cache.tvTime.setText(time);
		cache.tvZje.setText(record.zje);
		cache.tvDesc.setText("详情");

		cache.layoutContent.setBackgroundResource(R.drawable.tr_border);
		cache.tvLshh.setBackgroundResource(R.drawable.td_border);
		cache.tvTime.setBackgroundResource(R.drawable.td_border);
		cache.tvZje.setBackgroundResource(R.drawable.td_border);

		cache.tvLshh.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvTime.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvZje.setTextColor(context.getResources().getColor(
				R.color.black));
		cache.tvDesc.setTextColor(context.getResources().getColor(
				R.color.link_color));

		cache.tvDesc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((RecordActivity) context).queryDesc(record.lshh);
			}
		});
		return contentView;
	}

	class ViewCache {
		TextView tvLshh;
		TextView tvTime;
		TextView tvZje;
		TextView tvDesc;
		View layoutContent;
	}
}
