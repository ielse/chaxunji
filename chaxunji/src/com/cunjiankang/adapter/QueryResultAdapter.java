package com.cunjiankang.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cunjiankang.R;
import com.cunjiankang.bean.vo.QueryResult;
import com.cunjiankang.ui.MedicineQueryActivity;

/**
 * 
 * @ClassName: QueryResultAdapter
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:35:56
 * 
 */
public class QueryResultAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<QueryResult> list;
	private Context context;

	public QueryResultAdapter(Context context, List<QueryResult> list) {
		inflater = LayoutInflater.from(context);
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int poistion) {
		return list.get(poistion);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int poistion, View contentView, ViewGroup parent) {
		final QueryResult result = list.get(poistion);
		ViewCache cache = null;
		if (null == contentView) {
			contentView = inflater.inflate(R.layout.item_result, null);
			cache = new ViewCache();
			cache.tvSccj = (TextView) contentView.findViewById(R.id.tv_sccj);
			cache.tvSpmc = (TextView) contentView.findViewById(R.id.tv_spmc);
			cache.tvDesc = (TextView) contentView.findViewById(R.id.tv_desc);
			cache.tvSpec = (TextView) contentView.findViewById(R.id.tv_spec);
			cache.tvUnit = (TextView) contentView.findViewById(R.id.tv_unit);
			cache.layoutContent = contentView.findViewById(R.id.layout_content);
			contentView.setTag(cache);
		} else {
			cache = (ViewCache) contentView.getTag();
		}

		cache.tvSpmc.setText(null == result.spmc ? "" : result.spmc.trim());
		cache.tvSpec.setText(null == result.spec ? "" : result.spec.trim());
		cache.tvUnit.setText(null == result.unit ? "" : result.unit.trim());
		cache.tvSccj.setText(null == result.sccj ? "" : result.sccj.trim());
		cache.tvDesc.setText("详情");

		/*
		 * if(poistion==0){
		 * 
		 * cache.layoutContent.setBackgroundResource(R.color.tr_title);
		 * cache.tvSpmc.setBackgroundResource(R.drawable.th_border);
		 * cache.tvSccj.setBackgroundResource(R.drawable.th_border);
		 * cache.tvDesc.setBackgroundResource(R.drawable.th_border);
		 * 
		 * cache.tvSpmc.setTextColor(context.getResources().getColor(R.color.white
		 * ));
		 * cache.tvSccj.setTextColor(context.getResources().getColor(R.color.
		 * white));
		 * cache.tvDesc.setTextColor(context.getResources().getColor(R.color
		 * .white));
		 * 
		 * cache.tvDesc.setOnClickListener(null); }else{
		 */

		cache.layoutContent.setBackgroundResource(R.drawable.tr_border);
		cache.tvSpmc.setBackgroundResource(R.drawable.td_border);
		cache.tvSccj.setBackgroundResource(R.drawable.td_border);
		cache.tvDesc.setBackgroundResource(R.drawable.td_border);
		cache.tvSpec.setBackgroundResource(R.drawable.td_border);
		cache.tvUnit.setBackgroundResource(R.drawable.td_border);

		cache.tvSpmc.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvSccj.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvDesc.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvSpec.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvUnit.setTextColor(context.getResources()
				.getColor(R.color.black));

		cache.tvDesc.setTextColor(context.getResources().getColor(
				R.color.link_color));

		cache.tvDesc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MedicineQueryActivity) context).queryDetail(result.id);
			}
		});
		// }

		return contentView;
	}

	class ViewCache {
		TextView tvSccj;
		TextView tvSpmc;
		TextView tvDesc;
		TextView tvSpec;
		TextView tvUnit;
		View layoutContent;
	}
}
