package com.cunjiankang.adapter;

import java.util.List;

import org.kymjs.kjframe.KJBitmap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cunjiankang.R;
import com.cunjiankang.bean.vo.Goods;
import com.cunjiankang.ui.IntegralDHActivity;

/**
 * 
 * @ClassName: GoodsGVAdapter
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:32:38
 * 
 */
public class GoodsGVAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<Goods> list;
	private Context context;

	public GoodsGVAdapter(Context context, List<Goods> list) {
		this.context=context;
		inflater = LayoutInflater.from(context);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int poistion) {
		return list.get(poistion);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(final int poistion, View contentView, ViewGroup parent) {
		Goods goods = list.get(poistion);
		ViewCache cache = null;
		if (null == contentView) {
			contentView = inflater.inflate(R.layout.item_goods, null);
			cache = new ViewCache();
			cache.name = (TextView) contentView.findViewById(R.id.tv_name);
			cache.content = (TextView) contentView
					.findViewById(R.id.tv_content);
			cache.icon = (ImageView) contentView.findViewById(R.id.img_icon);
			cache.btnDH = (Button) contentView.findViewById(R.id.btn_dh);
			contentView.setTag(cache);
		} else {
			cache = (ViewCache) contentView.getTag();
		}
		cache.name.setText(goods.name);
		cache.content.setText(goods.content);
		KJBitmap kjb = KJBitmap.create();
		kjb.display(cache.icon, goods.img);
		contentView.findViewById(R.id.btn_dh).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if(context instanceof IntegralDHActivity){
					((IntegralDHActivity)context).print(poistion);
				}
			}
		});
		return contentView;
	}

	class ViewCache {
		TextView name;
		TextView content;
		ImageView icon;
		Button btnDH;
	}
}
