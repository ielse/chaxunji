package com.cunjiankang.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cunjiankang.R;
import com.cunjiankang.bean.vo.IntegralDetail;

/**
 * 
 * @ClassName: IntegralDetailAdapter
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:35:38
 * 
 */
@SuppressLint("SimpleDateFormat")
public class IntegralDetailAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<IntegralDetail> list;
	private Context context;
	private SimpleDateFormat sdf;

	public IntegralDetailAdapter(Context context, List<IntegralDetail> list) {
		inflater = LayoutInflater.from(context);
		this.list = list;
		this.context = context;
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int poistion) {
		return list.get(poistion);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int poistion, View contentView, ViewGroup parent) {
		IntegralDetail detail = list.get(poistion);
		ViewCache cache = null;
		if (null == contentView) {
			contentView = inflater.inflate(R.layout.item_integral_detail, null);
			cache = new ViewCache();
			cache.tvTime = (TextView) contentView.findViewById(R.id.tv_time);
			cache.tvJe = (TextView) contentView.findViewById(R.id.tv_je);
			cache.tvDesc = (TextView) contentView.findViewById(R.id.tv_desc);
			cache.layoutContent = contentView.findViewById(R.id.layout_content);
			contentView.setTag(cache);
		} else {
			cache = (ViewCache) contentView.getTag();
		}

		try {
			cache.tvTime.setText(new SimpleDateFormat("yyyy-MM-dd").format(sdf
					.parse(detail.kprq)));
		} catch (ParseException e) {
			cache.tvTime.setText(null == detail.kprq ? "" : detail.kprq.trim());
		}
		/*
		 * if(poistion==0){
		 * 
		 * cache.tvDesc.setText(detail.descc); cache.tvJe.setText(detail.ljje);
		 * 
		 * cache.layoutContent.setBackgroundResource(R.color.tr_title);
		 * cache.tvTime.setBackgroundResource(R.drawable.th_border);
		 * cache.tvJe.setBackgroundResource(R.drawable.th_border);
		 * cache.tvDesc.setBackgroundResource(R.drawable.th_border);
		 * 
		 * cache.tvTime.setTextColor(context.getResources().getColor(R.color.white
		 * ));
		 * cache.tvJe.setTextColor(context.getResources().getColor(R.color.white
		 * ));
		 * cache.tvDesc.setTextColor(context.getResources().getColor(R.color.
		 * white)); }else{
		 */
		cache.tvDesc.setText(null == detail.descc ? "" : detail.descc.trim());
		cache.tvJe.setText(null == detail.ljje ? "" : detail.ljje.trim());

		cache.layoutContent.setBackgroundResource(R.drawable.tr_border);
		cache.tvTime.setBackgroundResource(R.drawable.td_border);
		cache.tvJe.setBackgroundResource(R.drawable.td_border);
		cache.tvDesc.setBackgroundResource(R.drawable.td_border);

		cache.tvTime.setTextColor(context.getResources()
				.getColor(R.color.black));
		cache.tvJe.setTextColor(context.getResources().getColor(R.color.black));
		cache.tvDesc.setTextColor(context.getResources()
				.getColor(R.color.black));
		// }

		return contentView;
	}

	class ViewCache {
		TextView tvTime;
		TextView tvJe;
		TextView tvDesc;
		View layoutContent;
	}
}
