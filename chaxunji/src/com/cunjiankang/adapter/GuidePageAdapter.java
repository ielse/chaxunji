
package com.cunjiankang.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.cunjiankang.R;
import com.cunjiankang.bean.vo.HealthChartBean;
import com.cunjiankang.util.HealChatConstant;
import com.cunjiankang.view.ChartView;
import com.cunjiankang.view.DoubleDatePickerDialog.OnSelectScale;

import java.util.ArrayList;

/**
 * 
* @ClassName: GuidePageAdapter 
* @Description: 
* @author liyongqiang
* @date 2015-3-5 下午11:33:16 
*
 */
public class GuidePageAdapter extends PagerAdapter {
    private LayoutInflater mLayoutInflater;
    private int mCount;
    private Context mContext;
    private View mViews[];
	private TextView startText;
	private TextView endText;
    public ArrayList<HealthChartBean> mHealthChartBeans;
    private OnSelectScale onSelectScale;

    public GuidePageAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
        mContext = context;
    }

    public void setData(ArrayList<HealthChartBean> healthChartBeans) {
        mHealthChartBeans = healthChartBeans;
    }

    @Override
    public int getCount() {
        // return pageViews.size();
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
        mViews = new View[count];
        for (int i = 0; i < count; i++) {
            View view = mLayoutInflater.inflate(R.layout.item_chart, null);
            mViews[i] = view;
        }
    }

    public void clear() {
        mViews = null;
        mHealthChartBeans = null;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public Object instantiateItem(View container, int position) {
        HealthChartBean healthChartBean = mHealthChartBeans.get(position);
        float maxYValue = 0;
        float minYValue = 0;
        int yScaleCount = 0;
        ChartView chartView = (ChartView) mViews[position].findViewById(R.id.chart_view);
        startText = (TextView)(mViews[position].findViewById(R.id.startText));
		endText = (TextView) mViews[position].findViewById(R.id.endText);
		
		chartView.setOnSelectScale(onSelectScale);
        
        if (null != healthChartBean.yScaleValue && healthChartBean.yScaleValue.length > 0) {
            minYValue = healthChartBean.yScaleValue[0][0];
            for (float y[] : healthChartBean.yScaleValue) {
                if (y[y.length - 1] > maxYValue) {
                    maxYValue = y[y.length - 1];
                }
                // Logger.d(TAG, "y[0]=" + y[0]);
                if (y[0] < minYValue) {
                    minYValue = y[0];
                }
            }
            for (float y[] : healthChartBean.yScaleValue) {
                if ((maxYValue - minYValue) % (y.length - 1) == 0) {
                    yScaleCount = y.length;
                    break;
                }
                yScaleCount = y.length;
            }
        } else {
            chartView.setError();
            ((ViewPager) container).addView(mViews[position]);
            return mViews[position];
        }
        chartView.setScaleAttr(healthChartBean.time, healthChartBean.yScaleValue[0]);
        if(healthChartBean.time.length > 0 ){
        	startText.setText(healthChartBean.time[0]);
        	endText.setText(healthChartBean.time[healthChartBean.time.length - 1]);
        }
        if (null != healthChartBean.chartType) {
            for (int i = 0; i < healthChartBean.chartType.length; i++) {
                if (i > 2) {
                    break;
                }

                chartView.setLinearData(healthChartBean.dataValue[i],
                        mContext.getResources().getColor(HealChatConstant.LINE_COLOR_RESID[i]),
                        healthChartBean.indicatorName[i]);

            }
        }
        ((ViewPager) container).addView(mViews[position]);
        return mViews[position];
    }
    
	public OnSelectScale getOnSelectScale() {
		return onSelectScale;
	}

	public void setOnSelectScale(OnSelectScale onSelectScale) {
		this.onSelectScale = onSelectScale;
	}
}
