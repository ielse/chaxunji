package com.cunjiankang;

import android.app.Application;
import org.kymjs.kjframe.utils.CrashHandler;

/**
 * 
 * @ClassName: App
 * @Description:
 * @author liyongqiang
 * @date 2015-3-5 下午11:41:48
 * 
 */
public class App extends Application {

	private static App instance;

	@Override
	public void onCreate() {
		super.onCreate();

		instance = this;
		 CrashHandler.create(this);
	}

	public static synchronized App i() {
		return instance;
	}

}
